# Courseware as Code Site

This repository contains a template for **Courseware as Code**, which will allow you to easily generate a static website that can be deployed to Gitlab Pages for free.
Its main objective is to make hosting educational content as easy as possible.


[[_TOC_]]

## Sources

* The original [README.md][1] will show you how to setup your own local develoment environment (recommended: docker image).  Also, it will show you how to [fork][2] the project and how to start [customizing it][3].
* The [tutorial][4] will show you how to start adding your own content.
* And lastly, I've written a [small summary][5] listing the needed steps.


## Setup & publication

Both the [README.md][1] (or the [HOWTO][5]) will show you how to install docker, and how to fork the original [Courseware as Code][4] template.
After that, every time you push your changes to GitLab, a **CI/CD** pipeline will fire up, and in few minutes it will be available in your own public space.  For example, if you created the repository with the Gitlab user `luisg_muniz` and your repository is called `labs-curso-kubernetes`, the resulting URL will be: `https://luisg_muniz.gitlab.io/labs-curso-kubernetes`

You can customize how [Pandoc](https://pandoc.org/MANUAL.html) creates the slides by using the environment variables `CWARE_PANDOC_OPTS` and `CWARE_PANDOC_HEADERS`:
```bash
export CWARE_PANDOC_HEADERS="attached_files/slides_markdown/preamble.yml"
export CWARE_PANDOC_OPTS="--incremental --highlight-style=breezeDark"
export CWARE_PANDOC_OPTS="-V theme:metropolis -V themeoptions:numbering=fraction -V themeoptions:progressbar=foot"
docker-compose up
```

If you want to generate handouts:
```bash
export CWARE_PANDOC_OPTS="-V handout"
```

## Structure

After cloning, we'll need to customize:

- `_config.yml`
  - This is the main Jekyll setup file
  - Change, at least, the `title` and the `description`

- `index.md`
  - Main index.
  - Change `title` and `description`.  Get rid of the `nav_order`.  Maintain `permalink: /`
  - Include links to the slides (files with relative path `/slides_pdf/*.pdf`)

- `course/`
  - `00-index.md`
    - Just the header (`title`, `description`, `permalink: /labs` and `has_children: true`) but with no content at all.
  - `labs/` : Written with Jekyll and Kramdown.
    - `01-foo.md`
    - `02-bar.md`
    - ...
  - `attached_files/`
    - `slides_markdown/` : Written for Pandoc (heavily extended markdown including LaTeX/Beamer code)
      - `01-baz.md`
      - `02-bat.md`
      - ...

# Bugs

* Slides: I've been unable to add [backup slides](https://tug.org/docs/latex/beamertheme-metropolis/metropolistheme.pdf#17) (even tried this [solution](https://tex.stackexchange.com/a/411533))
* LaTeX babel package has no support for Spanish.
* Slides: When `(sub)sectionpage` is set to `progressbar`, the progress bar is not shown properly.

# References

For the labs:
* [Kramdown](https://kramdown.gettalong.org/quickref.html) is the Markdown flavor used.
* The original Jekyll template _was_ in [just-the-docs](https://pmarsceill.github.io/just-the-docs/), but now is **[here](https://github.com/just-the-docs/just-the-docs)**.
* [ES Liquid](https://github.com/Shopify/liquid/wiki/ES-Liquid-para-dise%C3%B1adores) can be useful. 

For the slides:
* [Pandoc](https://pandoc.org/MANUAL.html) User's Guide.
* The [Beamer](https://mirrors.ctan.org/macros/latex/contrib/beamer/doc/beameruserguide.pdf) class and its [Metropolis](https://tug.org/docs/latex/beamertheme-metropolis/metropolistheme.pdf) theme (including a nice [demo](https://github.com/matze/mtheme) for the latter).
* Metropolis needs the [FiraSans](https://github.com/bBoxType/FiraSans) fonts.


[1]: https://gitlab.com/courseware-as-code/courseware-template/-/blob/main/README.md
[2]: https://gitlab.com/courseware-as-code/courseware-template#by-forking-this-repository
[3]: https://gitlab.com/courseware-as-code/courseware-template#2-change-the-necessary-configurations
[4]: https://courseware-as-code.gitlab.io/courseware-tutorial/
[5]: https://bit.ly/40gtqfK
