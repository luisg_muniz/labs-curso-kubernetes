---
title: Kubernetes
subtitle: Introducción.  Certificaciones.
---
# Qué vamos a tratar

  * Certificación
  * Entornos de prácticas


# Certificación

  * <http://cncf.io>
    * Cloud Native Computing Foundation


# Exámenes

  * [CKA](https://www.cncf.io/training/certification/cka)
  * [CKAD](https://www.cncf.io/training/certification/ckad)
  * [CKS](https://www.cncf.io/training/certification/cks)
  * KCNA, KCSA, ICA, ...
    * ... no aplican aquí


# Exámenes: CKA

  * Comprensión _profunda_ de las tareas de administración típicas:

    * Mantenimiento del cluster
    * Mantenimiento de las redes
    * Tipos de almacenamiento
    * Resolución de problemas (_troubleshooting_) [&nbsp;](https://bit.ly/3sut4Hi)
      * Nodos
      * Infraestructura
      * Redes


# Exámenes: CKA

  * Examen **práctico**
    * 2 horas
    * De 17 a 20 tareas
      * O sea, 5-6 min/pregunta
  * 66% para aprobar
    * ...pero no sabemos cómo puntúa cada pregunta :-(
  * Dos intentos
    * Hay que dejar pasar al menos una semana para el segundo intento (_retake_)


# Exámenes: CKA

  * Custodiado (_Proctored_) [&nbsp;](https://bit.ly/47uQs6o)
    * Normas, requisitos, etc: <https://bit.ly/3slbq8Y>
  * Sobre ~~un~~ cluster**es** como el nuestro de prácticas
  * Sobre la [última versión](https://kubernetes.io/releases/) _disponible_
    * 1.28 a día de hoy
  * Se puede usar:
    * <http://docs.kubernetes.io>
    * <http://github.com/kubernetes/>
    * <http://blog.kubernetes.io>


# Temario (curricula)

  * <https://github.com/cncf/curriculum>
    * 25% Arquitectura del cluster, instalación y configuración
    * 15% Trabajos (_workloads_) y planificación
    * 20% Servicios y redes
    * 10% Almacenamiento
    * 30% Resolución de problemas


# Prerequisitos
  * Conceptos de arquitectura de Kubernetes
    * <https://www.cncf.io/training/courses/#introduction>
  * kubectl
  * vi(m), yaml, bash
  * (Docker)

::: notes
* Algunos recursos _gratuitos_ para complementar la formación:
  * Linux for Noobs: <https://bit.ly/3RiRtaK>
  * Practice Linux Commands: <https://bit.ly/3Y4KhRI>
  * Shell: <https://labex.io/skilltrees/shell>
  * Play with Docker Classroom:  <https://bit.ly/3XTlyjB>
:::

# Estrategia(s)

  * kubectl
  * doc
  * trucos
  * Ya llegaremos ;-)


# ¿Preguntas? {.plain}
\fin
