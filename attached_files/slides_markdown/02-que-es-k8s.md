---
title: Kubernetes
subtitle: Qué es Kubernetes
---
# Docker: funciona, pero ...

  * Gestionar múltiples contenedores es engorroso
  * No hay tolerancia a fallos
  * Volúmenes: asignación engorrosa
  * Puertos: asignación engorrosa
  * etc


\metroset{background=light}
# Docker: solución

![Necesitamos *orquestación*](attached_files/images/krustjuggle.png){width=128px height=168px}


# Orquestación: alternativas

  * Docker Swarm
  * Apache Mesos
    * ~~Mesosphere DC/OS~~ $\leadsto$ Konvoy
  * OpenShift
  * **Kubernetes** (k8s)[&nbsp;](https://bit.ly/40D5p3O)
  * Tanzu
  * ...


# Orquestación y microservicios

![Los microservicios pueden escalar de forma **independiente**](attached_files/images/microservicios-1alt.png){width=0.9\textwidth}


# Kubernetes permite ...

  * ... alta disponibilidad (cluster)
  * ... autoescalado
    * escalado **independiente**
  * ... conectar los contenedores
    * con el exterior (usuarios/as)
    * con otros contenedores (microservicios)
  * ... CI/CD
    * desarrollo **independiente**
  * ... Cloud, ecosistema _enterprise_

::: notes
* Además:
  * Facilita la gestión de:
    * volúmenes
    * puertos
    * interconectividad
  * _Enterprise_: filtrado, balanceadores, monitorización, distintas formas (políticas de despliegue), asignación/limitación de recursos, ...
  * Portabilidad
    * Nos abstrae de la infraestructura ...
    * ...con lo que nos podemos centrar en la _lógica_ de las aplicaciones
    * ...p.ej, **multi-cloud**
  * **Ideal para** adoptar desarrollo basado en **microservicios**
:::


# ¿Preguntas? {.plain}
\fin

::: notes
* Créditos de las imágenes:
  * http://www.juggling.org/pics/cartoons.html
  * https://www.sumerge.com/microservices-solutions
:::

<!-- Otras imágenes interesantes:
     * https://vuestorefront.io/blog/microservices
     * https://microservices.io/patterns/microservices.html
     * https://medium.com/javarevisited/top-10-microservice-design-patterns-for-experienced-developers-f4f5f782810e
-->
