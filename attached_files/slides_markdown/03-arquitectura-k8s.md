---
title: Kubernetes
subtitle: Arquitectura
---
# Kubernetes: conceptos

::: nonincremental
  * **Cluster**
  * Objetos
:::


# Cluster

  * Nodes (_minions_)
    * Donde ejecutan los trabajos (_workloads_)
  * Control Plane (nodos _master_)
    * Interactua con los interfaces
      * CLI: kubectl
      * GUI: Dashboard
      * ...


# Cluster

![<https://kubernetes.io/docs/concepts/overview/components>](attached_files/images/k8s-architecture.png)


# Master

::: nonincremental
  * Críticos \pause $\implies$ mínimo de 3 \pause
  * _split brain_ $\pause$ $\leftrightsquigarrow$ [Raft](https://en.wikipedia.org/wiki/Raft_(algorithm)) (algoritmo)
:::


# Master: por qué es crítico

  * etcd
    * Configuración del cluster
    * Distribuida
    * Clave/Valor
  * guarda _configuración_ **y** _estados_


# Master

  * Instalador (con kubeadm)
  * _Leader_ de etcd
  * [Container runtime](https://kubernetes.io/docs/concepts/containers/#container-runtimes) (OCI CRI)
    * cri-o, containerd, ~~rklet~~, ~~dockershim~~, ...
    * "Tainted"
  

# Lab: Revisar los componentes del _Control Plane_

  * [https://kubernetes.io/docs/concepts\newline/overview/components/#control-plane-components](https://kubernetes.io/docs/concepts/overview/components/#control-plane-components)


# Nodes

::: nonincremental
  * kubelet \pause $\leftrightsquigarrow$ kube-apiserver (_Control Plane_) \pause
  * kube-proxy \pause
:::

# Lab: Revisar los componentes del kubelet

  * [https://kubernetes.io/docs/concepts\newline/overview/components/#node-components](https://kubernetes.io/docs/concepts/overview/components/#node-components)
  * `kubectl get nodes `$\bigl[$`-o wide`$\bigr]$
  * `systemctl status kubelet`
  * `sudo systemctl enable --now kubelet`


# ¿Preguntas? {.plain}
\fin


# Más infraestructura: redes

  * Kubernetes **no** se encarga de la capa de red
  * Delega en _plugins_


# Más infraestructura: funciones de red necesarias

  * IP Address Management (IPAM)
    - asignar IPs a los pods
  * Pod networking (pod-to-pod)
    - comunicación de unos pod con otros, incluso entre nodos
  * Service-to-external
    - hacer públicos los _endpoints_
    - posiblemente, hacer distribución de tráfico/peticiones... (_load balancing_)
  * Network Policy Enforcent
    - garantizar seguridad y cumplimiento normativo
    - firewall, rutas, tagged traffic, ...


# Más infraestructura: normalización

  * CNI: **C**ontainer **N**etwork **I**nterface
    * Especificación sobre cómo deben integrarse las distintas soluciones que implementen la capa de red
    * "CNI defines the protocol between the plugin binary and the runtime"


# Más infraestructura: redes habituales

::: nonincremental
  * [Calico](https://projectcalico.org//)
  * [Flannel](https://github.com/coreos/flannel)
  * [KubeRouter](https://kube-router.io/)
  * [Cillium](https://cilium.io/)
:::


# ¿Preguntas? {.plain}
\fin

::: notes
* Créditos de las imágenes:
  * Google Cloud Platform
:::
