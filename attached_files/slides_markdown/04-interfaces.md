---
title: Kubernetes
subtitle: Interfaces
---
# Interfaces

  * Interactúan con el _Control Plane_
    * **kubectl**
    * Dashboard
    * API server


# kubectl: sintaxis

  ![](attached_files/images/kubectl-syntax.png)


# kubectl: ejemplos

::: nonincremental
  * \onslide<1->{kubectl }\onslide<2->{get }\onslide<3->{pod }\onslide<5->{mypod }\onslide<6->{\texttt{-o=wide}}
  * \onslide<1->{kubectl }\onslide<2->{get }\onslide<4->{pods }
  * \onslide<1->{kubectl }\onslide<2->{get }\onslide<4->{pods }\onslide<7->{\texttt{-o=yaml}}
:::


# kubectl: más ejemplos

  * `kubectl get pods --selector=app=nginx`
  * `kubectl get pod/mypod`
  * `kubectl <...> --help`


# ¿Dónde debemos instalar kubectl?

  \center{\textcolor{Mahogany}{\Huge{?}}}

  
# kubectl: previo

  * Primero, hay que configurar kubectl
  * $\sim$/.kube/config
  * kubectl config view
    * Nombre del _cluster_
    * IP y puerto
    * Credenciales


# Contextos

  * (1) × kubelet $\leadsto$ (n) × cluster
    * ...también vale con _namespaces_


# Contextos

  * kubectl config ...
    * ... get-clusters
    * ... get-contexts
    * ... current-context
    * ... use context <_xyz_>


# Dashboard

  * Interfaz gráfico...
    * ...que **no** usaremos ?:-)


# API

  * Permitirá consultas directas a cada contenedor
    * estado
    * documentación
    * estadísticas
    * ...


# API

  * <https://k9scli.io/>
  * [kubecraft](https://github.com/stevesloka/kubecraft)
  * [kubedoom](https://github.com/storax/kubedoom)
  * ...tampoco las usaremos :-(


# ¿Preguntas? {.plain}
\fin

::: notes
* Créditos de las imágenes:
  * Google Cloud Platform
:::
