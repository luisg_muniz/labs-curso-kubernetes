---
title: Kubernetes
subtitle: Objetos
---
# Kubernetes: conceptos

::: nonincremental
  * Cluster
  * **Objetos**
:::


# Kubernetes: definición

  * Cluster para servir apps en contenedores


# Kubernetes: objetos

  * Por medio de los objetos describimos cómo deben funcionar las apps
    * Cuántas copias de cada contenedor
    * Si una app debe ser accesible desde el exterior (internet)
    * Si una app debe ser accesible desde el interior (cluster)
    * Cómo substituir versiones antigüas de cada app por nuevas
    * Cómo distribuir las peticiones
    * ...


# Pods (po)

  * Uno o varios contenedores
  * Más su red
  * Más su almacenamiento


# Pods (po)

  ![<https://kubernetes.io/docs/concepts/workloads/pods/>](attached_files/images/pod-in-cluster.png)


# Pods (po)

  * [.../concepts/workloads/pods/#using-pods](https://kubernetes.io/docs/concepts/workloads/pods/#using-pods)
  * Todos los objetos tienen
    * ...un _nombre_
    * ...y UUID (autoasignado)


# Inciso: YAML

  ```yaml
  ---
  a
  k
  m
  s
  ...
  ```
        

# Inciso: YAML

  ```yaml
  ---
  a(lways)
  k(eep)
  m(y)
  s(anity)
  ...
  ```


# Inciso: YAML

  ```yaml
  ---
  apiVersion:
  kind:
  metadata:
  spec:
  ...
  ```

  
# Services (svc)

  * Permite que un pod sea accesible
  * [.../concepts/services-networking/](https://kubernetes.io/docs/concepts/services-networking/)
  * Introduce `type`


# Services: tipos

  * LoadBalancer
    * $\implies$ Desde Internet
    * $\implies$ A través de un _balanceador_ público
  * ClusterIP
    * $\implies$ Desde fuera del cluster
    * $\implies$ A través de una IP del cluster
  * NodePort
    * $\implies$ Desde fuera (o dentro) del cluster
    * $\implies$ A través de cualquier nodo
  * (ExternalName)


# Services: notas

  * En realidad, un service *no* _encapsula_ ...
    * ...sino que se _asocia_
    * ...a uno o más pods
      * $\implies$ `labels`
      * $\implies$ `selector`


# Replication Controller (rc)

  * En **desuso**
  * Garantiza que siempre haya (n) réplicas de un pod funcionando
    * Si un pod cae, lo vuelve a levantar
    * Permite reescalado (manual)


# Replication Controller (rc)

  * [.../concepts/workloads/controllers/replicationcontroller/](https://kubernetes.io/docs/concepts/workloads/controllers/replicationcontroller/)
  * Introduce `replicas`
  * Introduce `template`


# Recapitulemos

::: nonincremental
  * _pod_ \pause $\Leftrightarrow$ Aplicación(es) desplegada(s) ... \pause
  * _svc_ \pause $\Leftrightarrow$ ...y accesibles \pause
  * _rc_  \pause $\Leftrightarrow$ ...y monitorizada \pause (y escalable)
:::

# Recapitulemos

  * Para desplegar una **nueva versión** de la(s) app(s) ...
  * ...tendremos que hacer parada
  * Solución: _replicasets_ y _deployments_


# Deployment (deploy)

  * Crea implícitamente un _replicaset_ (_rs_)
  * [.../concepts/workloads/controllers/](https://kubernetes.io/docs/concepts/workloads/controllers/)
  * Introduce `Strategy`$\Large\boldsymbol{.}$`Type`
    * RollingUpdate
    * Recreate


# ¿Preguntas? {.plain}
\fin

::: notes
* Créditos de las imágenes:
  * Google Cloud Platform
:::
