---
title: Kubernetes
subtitle: Aún mas objetos
---
# Kubernetes: conceptos

::: nonincremental
  * Cluster
  * **Objetos**
:::


# pods: derivados

  * jobs
  * cronjobs


# jobs

  * Tareas que ejecutan **una única vez**
  * [.../concepts/workloads/controllers/job/](https://kubernetes.io/docs/concepts/workloads/controllers/job/)
  * Introduce `template`


# jobs: tipos

  * non-parallel (serie)
  * parallel
    * fixed-completion count
    * work-queue
  * cronjobs


# jobs: non-parallel

  * Tarea que ejecuta **una única vez**...
  * ...a no ser que falle 
  * $\bigl[$ `completions` $=$ 1 $\bigr]$, $\bigl[$ `parallelism` $=$ 1 $\bigr]$
  * Uso: Inicializaciones
    * Crear directorios, limpiar temporales, sync,  ETL ...


# jobs: parallel, fixed-completion count
  * La tarea está completa cuando se han ejecutado $(n)$ copias
  * `completions` $=$ $(n)$, `paralelism` = $(m)$
  * Uso: Distribuir trabajos
    * _Renderizado_ de escenas, ficheros, ...


# jobs: parallel, work queue
  * Tareas paralelas ...
  * ...pero se coordinan entre ellas o con un "patrón"
  * ...terminan cuando lo diga el "patrón"
  * `completions` = $\lambda$, `parallelism` = $(m)$
  * Uso: Colas de trabajos
    * Ticket system, call center, ...


# cronjobs
  * jobs que ejecutan periódicamente
  * [.../concepts/workloads/controllers/cron-jobs/](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/)
  * Introduce `schedule`
    * La periodicidad se marca como en una _crontab_ de Unix


# Dæmonsets (ds)

  * Una réplica del pod en cada nodo
  * [.../concepts/workloads/controllers/daemonset/](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/)
  * Uso:
    * Recolección de logs, stats, almacenamiento distribuido, ...
  

# Statefulsets (sts)

  * [.../concepts/workloads/controllers/statefulset/](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/)


# Headless services

  * [.../docs/concepts/workloads/controllers/statefulset/](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/)


# Pero ¿cuántos objetos hay?

  * kubectl api-resources


# ¿Preguntas? {.plain}
\fin

::: notes
* Créditos de las imágenes:
  * Google Cloud Platform
:::
