---
title: Kubernetes
subtitle: Namespaces
---
# Objetos: nomenclatura

* _Todos_ los objetos se identifican por su nombre
* Kubernetes les asigna un UUID al crearlos
* No puede haber nombres repetidos


# Objetos: nomenclatura

```yaml
apiVersion: apps/v1
kind: Pod
metadata:
  name: nginx
  uid: 4dd474fn-f389-11f8-b38c-42010a8009z7
[...]
```


# Namespaces: utilidad

* Crean "espacios de nombres" (_scopes_) distintos
* Separan los objetos del cluster
  * Por entorno (test, producción, staging, ...)
  * Por cliente y/o aplicación
  * Por función (admin, monitorización, app, ...)


# Namespaces: ejemplo

![](attached_files/images/namespaces-sample.png)


# Namespaces: por omisión

![](attached_files/images/namespaces-default.png)


# Namespaces: características

* Posibilitan objetos con nombre repetido
  * ...pero en distinto _scope_
* Separan recursos
  * DNS separados
  * Límites, cuotas, reservas independientes
  * _Network policies_
  * RBAC


# Namespaces: recomendaciones


![\ ](attached_files/images/drake-about-namespaces.png)


# ¿Preguntas? {.plain}
\fin
