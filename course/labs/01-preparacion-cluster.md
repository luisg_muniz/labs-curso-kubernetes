---
title: 01 Preparación de entornos
parent: Laboratorios
---
<details open markdown="block">
  <summary>Sumario</summary>
  {: .text-delta }
1. TOC
{:toc}
</details>



# Objetivo

Los laboratorios del curso van a hacerse creando un pequeño cluster local, con un único nodo _master_ y uno (o dos) nodos de ejecución.  En este primer laboratorio intentaremos instalarlo **en una máquina propia (local)**[^1].




# Prerequisitos

Para completar este laboratorio, necesitaremos:
* Ser capaces de editar un fichero de texto
* Instalar [Vagrant](https://www.vagrantup.com/)
* Instalar [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* Tener algún cliente `git` instalado
* En el caso de máquinas Windows, haber desactivado Hyper-V
* Descargar, configurar y desplegar el proyecto `vagrant-kubeadm-kubernetes`



# Pasos

## Descripción del problema

* La [instalación típica](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/) de un cluster de Kubernetes se hace partiendo de un primer nodo (que, típicamente, se convertirá en un nodo _master_).  Desde ahí lanzaremos el programa que permita configurar el resto de nodos del cluster.
* Hay [varias herramientas](https://kubernetes.io/docs/setup/production-environment/tools/) que permiten simplificar y automatizar la instalación del cluster[^2].
* De todas ellas, la más extendida es [kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/) y, de hecho, es la que nos van a exigir saber manejar en la certificación.
* Sin embargo, nosotros **no** vamos a usarla directamente porque **no** disponemos de varios nodos, bien sean físicos (_raw iron_ o _bare metal_), bien sean virtualizados (_Xen_, _Proxmox_, _VMWare_, ...).  Además, llevaría más tiempo del que ahora queremos emplear en esta formación.
* Por tanto, vamos a usar **otra** herramienta que va a simplificar mucho la instalación: [vagrant-kubeadm-kubernetes](https://github.com/techiescamp/vagrant-kubeadm-kubernetes)


<br>
<br>
## Editor(es) de texto

En este laboratorio, en todos los ejemplos se está usando el [editor](https://www.gnu.org/fun/jokes/ed-msg.html) [vi(m)](https://www.vim.org/) para editar el contenido de los ficheros.  En realidad, se puede instalar y usar cualquier otro editor o entorno de desarrollo alternativo:

* [GNU nano](https://www.nano-editor.org/)
* [Visual Studio](https://visualstudio.microsoft.com/)
* [Sublime Text](https://sublimetext.com)
* [Notepad++](https://notepad-plus-plus.org/)
* [IntelliJ IDEA](https://www.jetbrains.com/idea/)

**Sin embargo** lo recomendable es seguir empleando `vi(m)` (o `nano`):
* al ser basados en texto necesitan muchos menos recursos para funcionar y, lo que es más importante, arrancan inmediatamente.
* los editores que **seguro** vamos a tener en el entorno del examen de certificación son precisamente `vi(m)` y `nano`.
* los entornos de desarrollo ofrecen funcionalidades (p.ej, plantillas YAML) que **no** vamos a tener disponibles durante la certificación.



<br>
<br>
## Instalación de Vagrant

01. Descargamos e instalamos la herramienta `Vagrant`.  Podemos acceder a su [sitio web](https://hashicorp.com) para seguir las [instrucciones de instalación](https://developer.hashicorp.com/vagrant/install?product_intent=vagrant).  Ahí deberemos seleccionar nuestro Sistema Operativo y versión/distribución.

    * En el caso de una instalación en Windows, descargamos e instalamos el ejecutable.
    * En el caso de una instalación en macOS, es **muy** recomendable emplear [Homebrew](https://brew.sh/)
    * Finalmente, en el caso de usar Linux, las instrucciones nos indicarán qué repositorio añadir.  A partir de ahí, se hace la instalación del paquete necesario.

02. Una vez instalado Vagrant sería recomendable (¡pero no es obligatorio!) instalar los plugins relacionados con VirtualBox:

    ~~~shell
    $ vagrant plugin list
    [...]
    $ vagrant plugin install virtualbox vagrant-vbguest
    ~~~

<br>
<br>
## Instalación de VirtualBox

01. Si no lo tuviésemos ya, descargamos e instalamos [VirtualBox](https://www.virtualbox.org/wiki/Downloads) para nuestra plataforma.

    > **Nota**: Es _muy recomendable_ aprovechar e instalar asimismo las "VirtualBox Extension Pack" correspondientes a la versión de VirtualBox que tengamos instalada.


02. Si tenemos la versión 7.x de VirtualBox, y estamos trabajando en Linux, en principio vamos a tener un problema: las redes que se van a poder crear estarán muy limitadas.  Para corregirlo, ejecutaremos los siguientes comandos:

    ~~~shell
    $ sudo mkdir -p /etc/vbox
    $ echo "* 0.0.0.0/0 ::/0" | sudo tee -a /etc/vbox/networks.conf
    $ cat /etc/vbox/networks.conf
    * 0.0.0.0/0 ::/0
    ~~~


<br>
<br>
## Instalamos un cliente git (Linux)

* En el caso de una instalación en Windows, el propio Vagrant aporta el comando `git`.  Podemos saltar este paso.

* En caso de usar una distribución basada en Red Hat:
~~~shell
$ sudo dnf install git
~~~

* En caso de usar una distribución basada en Debian:
~~~shell
$ sudo apt install git
~~~

    > **Nota**: A partir de aquí indicaremos las instrucciones sólo para un tipo de distribución de Linux.


<br>
<br>
## Desactivación de Hyper-V (sólo Windows)

01.  Desactivamos, en Windows, el funcionamiento de Hyper-V.

     * Pulsamos "Inicio" > "Panel de control" > "Activar o desactivar las características de Windows".
     * En la ventana que aparece, **deseleccionamos** la casilla correspondiente a "Hyper-V".  Dependiendo de qué versión de Windows esté empleando tendrá que des-seleccionar adicionalmente "Plataforma del hipervisor de Windows".
![Desactivar Hyper-V](https://www.nakivo.com/blog/wp-content/uploads/2021/06/How-to-uninstall-Windows-10-Hyper-V-in-Control-Panel-.webp)
     * Pulse en "Aceptar" para confirmar los cambios.
     * Si el sistema le solicita reiniciar, hágalo ahora.

02.  Si hemos tenido que reiniciar en Windows, verificaremos la correcta desactivación:
     * Pulsamos la combinación de teclas [Win]+[R]
     * En el cuadro de diálogo que aparece ("Ejecutar") escribimos "msinfo32.exe" y pulsamos en "Aceptar"
     * Aparecerá la ventana de la aplicación "Información del sistema".
     * En el árbol de entradas de la izquierda nos cercioramos de haber seleccionado "Resumen del sistema"
     * En el panel de la derecha buscamos si hay información sobre el hipervisor (deberían ser las últimas entradas)
![Revisar Hyper-V](https://www.nakivo.com/blog/wp-content/uploads/2021/06/Hyper-V-is-installed-in-Windows.webp)



<br>
<br>
## Despliegue de `vagrant-kubeadm-kubernetes`

01. Descargamos el repositorio:

    ~~~shell
    $ git clone https://github.com/techiescamp/vagrant-kubeadm-kubernetes.git
    ~~~

    * En una máquina Linux habremos tenido que usar una _shell_.  En Windows habremos lanzado una _Powershell_ (*sin* privilegio de administración).


02. Personalizamos el cluster a usar

    * Para ello primero accederemos al directorio recién descargado y comprobaremos cuál es la situación inicial:

    ~~~shell
    $ cd vagrant-kubeadm-kubernetes
    $ vagrant status
    Current machine states:

    controlplane              not created (virtualbox)
    node01                    not created (virtualbox)
    node02                    not created (virtualbox)

    This environment represents multiple VMs. The VMs are all listed
    above with their current state. For more information about a specific
    VM, run `vagrant status NAME`.
    ~~~


    * Después editamos el fichero principal de configuración (`settings.yaml`) y localizamos las siguientes líneas:

    ~~~yaml
    [...]
    nodes:
      control:
        cpu: 2
        memory: 4096
    [...]
    ~~~

    > **Nota**: Tal y como se mencionaba en la sección [Editor(es) de texto](#editores-de-texto) podemos usar cualquier editor que se desee.


03. Para el nodo `control`, cambiamos el valor de `memory` a algo inferior (por ejemplo, basta con 2500).  Para los nodos `workers`, nos aseguramos de que el valor de `count` sea 2, y el valor de `memory` sea, por ejemplo, 1800.  Guardamos los cambios y cerramos el fichero.


04. Revisamos los cambios hechos:

    ~~~diff
    diff --git a/settings.yaml b/settings.yaml
    index 3041dda..13ae0a5 100644
    --- a/settings.yaml
    +++ b/settings.yaml
    @@ -19,11 +19,11 @@ network:
     nodes:
       control:
         cpu: 2
    -    memory: 4096
    +    memory: 2500
       workers:
         count: 2
         cpu: 1
    -    memory: 2048
    +    memory: 1800
     # Mount additional shared folders from the host into each virtual machine.
     # Note that the project directory is automatically mounted at /vagrant.
     # shared_folders:
    ~~~


05. Lanzamos la creación del entorno:

    ~~~shell
    $ vagrant up
    [...]
    ~~~

    > **Atención**: La generación de las máquinas típicamente tardará bastantes minutos (¡de 10 a 20 minutos!) y puede provocar bastante tráfico de red.


06. Cuando termine el proceso comprobaremos, y nos conectaremos al nodo "master":

    ~~~shell
    $ vagrant status
    Current machine states:

    controlplane              running (virtualbox)
    node01                    running (virtualbox)
    node02                    running (virtualbox)

    This environment represents multiple VMs. The VMs are all listed
    above with their current state. For more information about a specific
    VM, run `vagrant status NAME`.

    $ vagrant ssh controlplane
    Welcome to Ubuntu 22.04.3 LTS (GNU/Linux 5.15.0-83-generic x86_64)
    [...]
    vagrant@controlplane:~$ kubectl version
    Client Version: v1.29.0
    Kustomize Version: v5.0.4-0.20230601165947-6ce0bf390ce3
    Server Version: v1.29.5
    vagrant@controlplane:~$ exit
    ~~~

    Nótese que la conexión se establece sin necesidad de tener que teclear la contraseña (_passwordless authentication_).

    > **Nota**: Si la instalación no finalizó correctamente, puede reintentarla una (o incluso dos) veces más; si vuelve a ejecutar `vagrant up` se intentará continuar la instalación.  Si aún así no llega a completar correctamente, o no se llega a conectar, coméntelo con el instructor.



<br>
<br>
## Operativa:

   Se puede operar con las máquinas del cluster empleando el interfaz gráfico de VirtualBox.  Sin embargo, será más cómodo emplear el comando `vagrant`.

   Operaciones habituales:
   * `vagrant halt [<máquina>]`:  Para detener todas las máquinas virtuales, o bien sólo la máquina indicada.
   * `vagrant up [<máquina>]`:  Para levantar todas las máquinas virtuales, o bien sólo la indicada
   .  Si alguna está sin instalar, automáticamente se intentará completar la instalación.
   * `vagrant destroy [<máquina>]`:  Para destruir todas las máquinas virtuales, o bien sólo la máquina indicada.
   * `vagrant ssh <máquina>`: Para acceder, por medio de SSH, a la máquina indicada.  La conexión se hará automáticamente, sin necesidad de teclear la contraseña de la cuenta `vagrant`.

   > **Nota**: Si sospechamos que alguna máquina no ha sido correctamente instalada, tenemos un par de opciones:
   * Repetir la post-configuración (provisionamento).  Para ello usaremos `vagrant up <máquina> --provision` o bien `vagrant provision <máquina>`, según la `<máquina>` esté ya levantada o no.
   * Destruir la `<máquina>` (`vagrant destroy ...`) y volver a crearla (`vagrant up ...`)




# Extra

Si nos apetece (¡**y** nos sobra tiempo!) y disponemos de otro sistema podemos repetir la instalación una segunda vez; podemos aprovechar para indicar otro número de "nodes" o incluso usar otra versión de Kubernetes (hay que buscarla en `settings.yaml`)

Pensando en el [examen de certificación (CKA)](https://www.cncf.io/training/certification/cka/) ¿qué versión de Kubernetes cree que sería [adecuada](https://kubernetes.io/releases/) indicar en `settings.yml`?



# Conclusiones

Al finalizar este laboratorio, debería tener ya un entorno listo para poder realizar sus propias prácticas con vistas a la certificación.



<center>— ♠ —</center>

<br>
<br>
## Notas a pie de página

[^1]: Nos interesa tener preparado el entorno local para poder preparar la certificación por nuestra cuenta.
[^2]: En realidad también se puede hacer la [instalación del cluster paso a paso](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#installing-runtime).  En realidad es de escasa utilidad (y hay mejores formas de gastar el tiempo) ... pero como es muy detallada se acaba aprendiendo bien cómo se interrelacionan los componentes.
