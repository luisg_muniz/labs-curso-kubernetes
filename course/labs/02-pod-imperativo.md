---
title: 02 Pod simple (por comando)
parent: Laboratorios
---
<details open markdown="block">
  <summary>Sumario</summary>
  {: .text-delta }
1. TOC
{:toc}
</details>



# Objetivo

Empezaremos creando un primer _pod_ muy sencillo, usando una imagen de ejemplo.  Esa [imagen](https://hub.docker.com/_/nginx) es un servidor web [nginx](http://nginx.com) que debería mostrarnos una [página simple de bienvenida](http://hg.nginx.org/nginx/raw-file/tip/docs/html/index.html).

Practicaremos la creación de objetos (un _pod_, en este caso) de forma imperativa (es decir, únicamente por comando).  En el próximo laboratorio se usará la forma declarativa.



# Pasos


## Creación por comando ("configuración imperativa")

01. Usaremos el comando `kubectl` para crear un primer pod de la forma más directa:

    ~~~shell
    $ kubectl run nginx-pod --image=docker.io/library/nginx:latest
    pod/nginx-pod created
    ~~~


02. Comprobamos la creación:

    ~~~shell
    $ kubectl get pods
    NAME        READY   STATUS    RESTARTS   AGE
    nginx-pod   1/1     Running   0          27s

    $ kubectl get pod nginx-pod
    NAME        READY   STATUS    RESTARTS   AGE
    nginx-pod   1/1     Running   0          34s

    $ kubectl get pod nginx-pod -o wide
    NAME        READY   STATUS    RESTARTS   AGE   IP             NODE            NOMINATED NODE   READINESS GATES
    nginx-pod   1/1     Running   0          43s   172.16.158.6   worker-node02   <none>           <none>
    ~~~


03. Cada pod tendrá su propio _log_ separado:

    ~~~shell
    $ kubectl logs nginx-pod
    /docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
    /docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
    [...]
    ~~~


04. Podemos incluso acceder al pod ejecución.

    ~~~shell
    $ kubectl exec nginx-pod -- uname -a
    Linux nginx-pod 5.15.0-83-generic #92-Ubuntu SMP Mon Aug 14 09:30:42 UTC 2023 x86_64 GNU/Linux

    $ kubectl exec nginx-pod -- lsmem
    RANGE                                 SIZE  STATE REMOVABLE BLOCK
    0x0000000000000000-0x000000007fffffff   2G online       yes  0-15

    Memory block size:       128M
    Total online memory:       2G
    Total offline memory:      0B
    ~~~

    En caso de necesitar una shell interactiva tendremos que indicárselo al comando `kubectl exec`:
    * `-i` ⇒ sesión interactiva
    * `-t` ⇒ asignar un terminal)

    ~~~shell
    $ kubectl exec -it nginx-pod -- /bin/bash
    root@nginx-pod:/# ls; date; pwd
    [...]
    [...y todo lo que queramos ejecutar...]
    [...]
    root@nginx-pod:/# ps
    bash: ps: command not found
    root@nginx-pod:/# top
    bash: top: command not found
    root@nginx-pod:/# ls -l /proc/*/exe
    [...]
    root@nginx-pod:/# exit
    ~~~


    {% quiz %}
    ---
    locale: es
    ---
    # ps / top / etc
    Descontando la shell necesaria para la sesión interactiva, ¿cuántos procesos hay ejecutando en el contenedor?

    1. [ ] Ninguno, por ser un contenedor `docker`
       > ¡Uy, casi! [&nbsp;&nbsp;](https://www.youtube.com/watch?v=YfDhGy5N000&t=332s)! >:-/  Prueba otra vez.
    1. [ ] Unos cien, que son los típicos en cualquier Linux sin interfaz gráfico
       > No.  Prueba otra vez.
    1. [x] Sólo uno: el servidor "nginx"
       > ¡Correcto!  Por eso mismo se dice que, en términos de eficiencia, un contenedor docker se compara a tener un proceso más
    {% endquiz %}


05. De momento, dejaremos aún este pod en ejecución para usarlo en el siguiente laboratorio.



# Conclusiones

Complete el siguiente laboratorio [03 Pod simple (por fichero)](../03-pod-declarativo/) antes de comparar los resultados.


<center>— ♠ —</center>
