---
title: 03 Pod simple (por fichero)
parent: Laboratorios
---
<details open markdown="block">
  <summary>Sumario</summary>
  {: .text-delta }
1. TOC
{:toc}
</details>



# Prerequisitos

Antes de iniciar este laboratorio es necesario haber completado el previo, [02 Pod simple (por comando)](../02-pod-imperativo/)



# Objetivo

Este laboratorio en realidad completa el anterior.  También crearemos un _pod_ con un servidor web nginx, pero esta vez de forma declarativa.



# Pasos


## Creación por fichero ("configuración declarativa")

01. La configuración que necesitamos es:

    ~~~yaml
    ---
    apiVersion: v1
    kind: Pod
    metadata:
      name: web2-pod
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx-container
        image: docker.io/library/nginx:1.23
        ports:
        - name: nginx-port
          containerPort: 80
    ...
    ~~~

    * Nótese que el nombre del objeto ("web2-pod") es distinto del pod anterior ("nginx-pod").  Kubernetes no permite que haya dos objetos con el mismo nombre[^1].




02. Guardamos esa configuración en, p.ej, un fichero llamado "pod-1.yaml":
    ~~~shell
    $ nl pod-1.yaml
    [...]
    ~~~


03. _Podríamos_ hacer ya mismo la creación del pod.
    Sin embargo, vamos a tratar antes un problema que podría llegar a sucedernos: en la práctica, no es raro que cometamos errores sintácticos al crear estos ficheros.  Estos pequeños errores pueden ser difíciles de localizar y hacer que perdamos mucho tiempo.

    Vamos a usar `yamllint`, que es un verificador sintáctico ("linter") para ficheros YAML[^2] [^3]

    ~~~shell
    $ sudo apt install -y yamllint
    [...]
    $ yamllint pod-1.yaml
    pod-1.yaml
      11:6      warning  missing starting space in comment  (comments)
    ~~~

04. En realidad, `yamllint` se estará quejando de que el fichero `pod-1.yaml` no es estrictamente un YAML 100% correcto ... y sin embargo, ese mismo fichero es suficientemente válido para Kubernetes.


    {% quiz %}
    ---
    locale: es
    ---
    # yamllint se queja
    ¿Qué deberíamos hacer? \
    Pista: Pulsa en la bombilla

    > En realidad, no hay una única respuesta válida, sino que la decisión correcta depende más bien de las circunstancias concretas

    1. [ ] Ignorar el aviso y continuar
       > Es válido ... pero, si podemos dedicarle un par de minutos, hay otra opción mejor
    1. [ ] Cambiar el contenido del fichero YAML hasta que `yamllint` ejecute limpiamente
       > Este es precisamente el peligro de los "linter" ... nos acaben haciendo perder más tiempo del que ahorra :-(
    1. [x] Personalizar `yamllint` para que se adapte a nuestro estilo de escritura
        > ¡Correcto!  El objetivo **no es** que `yamllint` no se queje, sino que se queje sólo de los errores importantes
    {% endquiz %}


    **Si queremos** podemos emplear la configuración personalizada que está en <https://pastebin.com/raw/nkVcqdFD>.
    Guardamos esa configuración (p.ej, en un fichero oculto llamado `.yamllint`) en el mismo directorio que los ficheros a comprobar.

    ~~~shell
    $ nl .yamllint
    [...]
    ~~~


05. Verificamos.  Primero, sintácticamente:

    ~~~shell
    $ yamllint pod-1.yaml
    ~~~

    _No news, good news_


06. ...y después, semánticamente.  Haremos que el comando `kubectl` simule la ejecución (`--dry-run`):

    ~~~shell
    $ kubectl create -f pod-1.yaml --dry-run=client
    pod/nginx-pod created (dry run)
    ~~~


07. A continuación, creamos y revisamos:

    ~~~shell
    $ kubectl create -f pod-1.yaml
    pod/web2-pod created

    $ kubectl get pods
    NAME        READY   STATUS    RESTARTS   AGE
    nginx-pod   1/1     Running   0          32m
    web2-pod    1/1     Running   0          32s
    ~~~

    {% quiz %}
    ---
    locale: es
    ---
    # yamllint en el examen de certificación
    En el día a día se recomienda intentar detectar (y corregir) los errores lo antes posible, y por eso usamos `yamllint`, `--dry-run`, etc.  Pero, si estamos haciendo el examen de certificación, ¿cree que merece la pena hacer esas comprobaciones?

    1. [ ] No
       > No queremos "perder tiempo".  Sólo se justificaría (al fin y al cabo, estamos hablando de usar `--dry-run` y/o hacer `apt install -y yamllint`) si nos hemos quedado atascados/as en alguna de las tareas del examen de certificación.
    1. [ ] Noooo
       > No queremos "perder tiempo".  Sólo se justificaría (al fin y al cabo, estamos hablando de usar `--dry-run` y/o hacer `apt install -y yamllint`) si nos hemos quedado atascados/as en alguna de las tareas del examen de certificación.
    1. [x] NI-DE-COÑ*\*\*se lo llevan preso\*\**
       > No queremos "perder tiempo".  Sólo se justificaría (al fin y al cabo, estamos hablando de usar `--dry-run` y/o hacer `apt install -y yamllint`) si nos hemos quedado atascados/as en alguna de las tareas del examen de certificación.
    {% endquiz %}


## Acceso a los pods

01. Por cierto, cada pod tiene una dirección IP propia:

    ~~~shell
    $ kubectl get pods -o wide
    NAME        READY   STATUS    RESTARTS   AGE   IP             NODE            NOMINATED NODE   READINESS GATES
    nginx-pod   1/1     Running   0          93m   172.16.158.7   worker-node02   <none>           <none>
    web2-pod    1/1     Running   0          61m   172.16.158.8   worker-node02   <none>           <none>
    ~~~

02. Esto es una característica de diseño de Kubernetes.  Se quiso hacer de esta manera para que se pudiesen trasladar más fácilmente las aplicaciones que ya estaban funcionando en entornos "tradicionales"[^4]

    Supongamos por ejemplo una solución basada en una arquitectura a tres niveles.  Era típico tener tres equipos, cada uno con su sistema operativo y su dirección IP:
    * un servidor web, que conectaba con
    * un servidor de aplicaciones, que almacenaba en
    * un servidor dedicado como base de datos

    El _porting_ a Kubernetes puede ser muy directo: cada servidor se encapsula en un pod distinto.  Como cada pod tiene una IP, sólo habrá que reconfigurar cada aplicativo para que acceda a las otras máquinas usando la nueva IP[^5].

03. Desde cualquier máquina del cluster accedemos a esas IPs:

    ~~~shell
    $ sudo apt install -y links
    [...]

    $ links http://172.16.158.7

    $ links http://172.16.158.8
    ~~~

    * Podemos salir del navegador en modo texto [Links](https://youtu.be/Ph-CA_tu5KA?si=hTK6tHJx6D5ZM7sG) pulsando "q", o también pulsando "Esc" para acceder a los menús.


04. Todavía mantendremos esos dos pods para el próximo laboratorio.



# Conclusiones

El pod es la unidad mínima de trabajo de Kubernetes.  Así conseguimos el despliegue de las aplicaciones.

Los objetos se pueden crear mediante un fichero descriptivo YAML (y, de hecho, es así como se almacenan internamente).  Sin embargo, en ocasiones es más rápido usar un comando directo para conseguir el mismo resultado ... y es algo que deberíamos practicar para afrontar correctamente la certificación.

Aunque es posible acceder directamente a los pods no es una opción muy flexible (sólo tendrán acceso otros pods que ejecuten en el mismo cluster).  En el [próximo laboratorio](../04-acceso-a-pods/) veremos cómo ofrecer otro tipo de accesos.




<center>— ♠ —</center>

<br>
<br>
## Notas a pie de página

[^1]: No en el mismo "namespace", al menos.

[^2]: Tal y como se mencionó en el [laboratorio previo](../01-preparacion-cluster#editores-de-texto), otra posible alternativa sería emplear un entorno de desarrollo más potente.<BR>Al crear un fichero con extensión `.yml` / `.yaml` son capaces de ir detectando, en vivo, si el fichero tiene errores.  Incluso algunos editores generan el fichero con un esqueleto inicial y simplemente tendríamos que ir completando/borrando esa plantilla.

[^3]: `yamllint` no está disponible para instalar en entorno Windows, pero se puede hacer funcionar como módulo Python bajo MobaXterm: <https://bit.ly/3toxf7x>

[^4]: Esto es, en máquina física (_bare metal_) o bien virtualización de tipo I (VMWare ESX, kvm, ...) o de tipo II (VirtualBox, VMWare, ...)

[^5]: En realidad, también se puede hacer sin cambiar la configuración.  Basta con, en el momento de creación de los pod, solicitar IPs concretas (esto es, las IPs "antigüas").