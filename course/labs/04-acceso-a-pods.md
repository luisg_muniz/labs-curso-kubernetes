---
title: 04 Acceso a los pods
parent: Laboratorios
---
<details open markdown="block">
  <summary>Sumario</summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

# Prerequisitos

Antes de iniciar este laboratorio es necesario haber completado los dos previos:
* [02 Pod simple (por comando)](../02-pod-imperativo/), y
* [03 Pod simple (por fichero)](../03-pod-declarativo/)



# Objetivo

Comprobar cómo se puede ganar acceso a un _pod_ mediante una definición de servicio.  Además, comprobaremos cómo obtener más información de cada objeto.



# Pasos

## Información de los servicios

01. Comprobamos el estado inicial

    ~~~shell
    $ kubectl get pods
    NAME        READY   STATUS    RESTARTS   AGE
    nginx-pod   1/1     Running   0          139m
    web2-pod    1/1     Running   0          107m
    ~~~

02. Podemos obtener una explicación sobre qué es cualquier objeto.  Por ejemplo, un pod:

    ~~~shell
    $ kubectl explain pod
    KIND:       Pod
    VERSION:    v1

    DESCRIPTION:
    Pod is a collection of containers that can run on a host. This resource is
    created by clients and scheduled onto hosts.

    FIELDS:
    [...]
    ~~~

    > **Nota**: ¿Se le ocurre en qué **situación concreta** puede ser útil este comando?


03. También podemos obtener información muy detallada sobre el estado actual de cada objeto:

    ~~~shell
    $ kubectl describe pod web2-pod
    Name:             web2-pod
    Namespace:        default
    [...]
    Node:             worker-node02/10.0.0.12
    Start Time:       Wed, 15 Nov 2023 03:09:33 +0000
    Labels:           app=nginx
    [...]
    Status:           Running
    IP:               172.16.158.8
    [...]
    Containers:
      nginx-container:
        Image:          nginx:1.23
        Port:           80/TCP
        Host Port:      0/TCP
    [...]
    Events:
      Type    Reason     Age    From               Message
      ----    ------     ----   ----               -------
      Normal  Pulling    3m37s  kubelet            Pulling image "nginx:1.23"
      Normal  Scheduled  3m33s  default-scheduler  Successfully assigned default/web2-pod to worker-node02
    [...]
    ~~~



## Creación del servicio

01. Vamos a hacer que el pod web2-pod sea accesible desde fuera del cluster.  Para ello crearemos (de forma imperativa) un servicio.  Al ser de tipo "NodePort", el pod web2-pod será accesible en cualquier nodo del cluster:

    ~~~shell
    $ kubectl expose pod web2-pod --type=NodePort --name web2-svc
    service/web2-svc exposed

    $ kubectl get svc
    NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
    kubernetes   ClusterIP   172.17.0.1     <none>        443/TCP        23h
    web2-svc     NodePort    172.17.38.45   <none>        80:30668/TCP   4m34s

    $ kubectl describe svc web2-svc
    Name:                     web2-svc
    Namespace:                default
    [...]
    IP:                       172.17.38.45
    IPs:                      172.17.38.45
    Port:                     <unset>  80/TCP
    TargetPort:               80/TCP
    NodePort:                 <unset>  30668/TCP
    [...]
    ~~~

    > **Nota**: El puerto asignado aleatoriamente estará entre el 30_000 y el 32_767 (32k).  Aunque, si se hubiese necesitado, también se podía haber solicitado uno concreto.  En este ejemplo, el puerto asignado ha sido el 30_668.


02. Comprobamos la configuración de los nodos:

    ~~~shell
    $ kubectl get nodes -o wide
    NAME            STATUS   ROLES           AGE   VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
    master-node     Ready    control-plane   23h   v1.27.1   10.0.0.10     <none>        Ubuntu 22.04.3 LTS   5.15.0-83-generic   cri-o://1.27.1
    worker-node01   Ready    worker          23h   v1.27.1   10.0.0.11     <none>        Ubuntu 22.04.3 LTS   5.15.0-83-generic   cri-o://1.27.1
    worker-node02   Ready    worker          23h   v1.27.1   10.0.0.12     <none>        Ubuntu 22.04.3 LTS   5.15.0-83-generic   cri-o://1.27.1
    ~~~


03. **Desde la máquina anfitriona** (esto es, donde habíamos lanzado el comando `vagrant up`) abrimos un navegador.
    Accedemos a la(s) IP(s) de cada nodo (mostrada en el comando `... get nodes`), y al puerto mostrado en el comando `describe`.

    En el ejemplo actual:
    * http://10.0.0.11:30668
    * http://10.0.0.12:30668

    Debe(n) aparecer la(s) página(s) de bienvenida del servidor web nginx.  Si no es así, coméntelo con el instructor.

    > **Nota**: En el laboratorio anterior también llegábamos a ver la página web servida por el pod, por medio de la IP propia de cada pod.  ¿Puede justificar en qué sentido es mejor usar un servicio (HostPort, en este caso) en vez de usar la IP propia de cada pod?

## Limpieza

01. Borramos, por fin, los objetos creados en estos últimos tres laboratorios:

    ~~~shell
    $ kubectl get pods,svc
    NAME            READY   STATUS    RESTARTS   AGE
    pod/nginx-pod   1/1     Running   0          173m
    pod/web2-pod    1/1     Running   0          141m

    NAME                 TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
    service/kubernetes   ClusterIP   172.17.0.1     <none>        443/TCP        23h
    service/web2-svc     NodePort    172.17.38.45   <none>        80:30668/TCP   24m

    $ kubectl delete pod nginx-pod
    pod "nginx-pod" deleted
    $ kubectl delete -f pod-1.yaml   ### O bien "kubectl delete pod web2-pod"
    pod "web2-pod" deleted
    $ kubectl delete svc web2-svc
    service "web2-svc" deleted

    $ kubectl get pods,svc
    NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
    service/kubernetes   ClusterIP   172.17.0.1   <none>        443/TCP   23h
    ~~~



# Extra

En este laboratorio se han usado las IPs de los nodos.  En el anterior, las de los pods concretos.

¿Sería capaz de localizar de dónde salen estas IPs?  ¿Se podrían personalizar y, en su caso, reservar?



# Conclusiones

Al "exponer" un pod, estamos consiguiendo que ese pod sea accesible de cierta manera determinada.  Aunque hemos usado el modo "NodePort", hay otros modos de exposición que ya se verán.

También hemos visto más formas de sacar información detallada de los distintos objetos.

Nótese que, mientras que pod web2-pod fue creado de forma declarativa (por fichero), el servicio web2-svc fue creado de forma imperativa (por comando).  En próximos laboratorios se aglutinará la definición de ambos tipos de objetos (y alguno más) en el mismo fichero YAML.
<center>— ♠ —</center>
