---
title: 04b Más información (opcional)
parent: Laboratorios
---
<details open markdown="block">
  <summary>Sumario</summary>
  {: .text-delta }
1. TOC
{:toc}
</details>


# Objetivo

Aprenderemos a extraer información detallada de los distintos objetos que maneja Kubernetes.  Llegaremos incluso a poder cambiar la representación de los datos de la manera que nos interese; hasta podremos tener nuestros propios formatos de informes.



# Pasos

## Acceso a un primer pod

01. Creamos un primer pod.
    Si es necesario, podemos consultar esta [referencia de kubectl](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#expose).

    Empezaremos a manejar etiquetas (_labels_), aunque en realidad su verdadera utilidad se aprovechará en próximos laboratorios.

    ~~~shell
    $ kubectl run mipod --image nginx --labels "env=prod,app=web" --port 80
    pod/mipod created

    $ kubectl get pods -o wide --show-labels
    NAME    READY   STATUS    RESTARTS   AGE   IP              NODE            NOMINATED NODE   READINESS GATES   LABELS
    mipod   1/1     Running   0          26s   172.16.87.193   worker-node01   <none>           <none>            app=web,env=prod
    ~~~

    


02. Comprobamos, a través de la dirección IP asignada al pod (en este caso, 172.16.87.193), que el servidor web funciona correctamente:

    ~~~shell
    $ links http://172.16.87.193:80
    [...]
    ~~~


03. Creamos un servicio de tipo NodePort:

    ~~~shell
    $ kubectl expose pod mipod --type NodePort
    service/mipod exposed

    $ kubectl get all
    NAME        READY   STATUS    RESTARTS   AGE
    pod/mipod   1/1     Running   0          6m44s

    NAME                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
    service/kubernetes   ClusterIP   172.17.0.1      <none>        443/TCP        18h
    service/mipod        NodePort    172.17.23.202   <none>        80:30296/TCP   103s
    ~~~

    Como no hemos especificado un nombre, el servicio tendrá el mismo que el del pod.
    

04. Desde el anfitrión abrimos una ventana en un navegador para acceder de nuevo al servidor web.  Sin embargo, esta vez estaremos accediendo a través de la IP de un nodo (p.ej, 10.0.0.11) y el puerto que se nos ha asignado aleatoriamente (30_296, en este caso).


<BR>
<BR>
## Uso de `jq`

01. Si no lo está previamente, instalaremos el programa `jq`:

    ~~~shell
    $ which jq
    $ sudo apt update
    [...]
    $ sudo apt install -y jq
    [...]
    $ which jq
    /usr/bin/jq
    $ whatis jq
    jq (1)               - Command-line JSON processor
    ~~~


02. Vamos a extraer algo de información del pod.  Al indicar [formato JSON](https://en.wikipedia.org/wiki/JSON) se volcará toda la información del objeto (no sólo un pequeño resumen, como en el caso por defecto).<BR>
Filtraremos esa información a través de `jq`:

    ~~~shell
    $ kubectl get pods -l app=web -o json | jq ".items[].spec.containers[]"
    {
      "image": "nginx",
      "imagePullPolicy": "Always",
      "name": "mipod",
      "ports": [
        {
          "containerPort": 80,
          "protocol": "TCP"
        }
      ],
      "resources": {},
      "terminationMessagePath": "/dev/termination-log",
      "terminationMessagePolicy": "File",
      "volumeMounts": [
        {
          "mountPath": "/var/run/secrets/kubernetes.io/serviceaccount",
          "name": "kube-api-access-2q6fq",
          "readOnly": true
        }
      ]
    }

    $ !! | jq ".image"
    "nginx"
    ~~~

    Nótese que `!!` significa "repetición del último comando".  Y que, a los efectos de `jq`, es lo mismo indicar:
    * `jq ".items[].spec.containers[]" | jq ".image"`
    * `jq ".items[].spec.containers[].image"`

    
<BR>
<BR>
## Contenedores docker

01. Vamos a comprobar en qué nodo está ejecutando el pod:

    ~~~shell
    $ kubectl get pods -l app=web -o json | jq ".items[0] | .spec.nodeName | ascii_upcase"
    "WORKER-NODE01"
    ~~~

02. Abrimos una sesión en el nodo y comprobamos qué imágenes docker, y qué contenedores en ejecución, hay.  Al estar configurado [CRI-O](https://cri-o.io/) como _Container Runtime_, tendremos que usar el comando `crictl`:

    ~~~shell
    > vagrant ssh node01
    vagrant@worker-node01:~$ sudo crictl images
    IMAGE                                      TAG                 IMAGE ID            SIZE
    docker.io/library/nginx                    latest              c20060033e06f       191MB
    [...]

    vagrant@worker-node01:~$ sudo crictl ps -l
    CONTAINER           IMAGE                                         CREATED             STATE               NAME                ATTEMPT             POD ID              POD
    27af97f94ea66       docker.io/library/nginx@sha256:86e53c4[...]   9 hours ago         Running             mipod               1                   bfc1dea8fd9bd       mipod

    vagrant@worker-node01:~$ exit
    ~~~




<BR>
<BR>
## Templates

Otra forma de acceder a la información es mediante plantillas del [lenguage "Go"](https://go.dev).

Aunque siempre podemos consultar la [documentación oficial](https://golang.org/pkg/text/template/) también hay unos cuántos tutoriales que pueden resultar interesantes:
* [Customizing OC Output with go-templates (Red Hat)](https://cloud.redhat.com/blog/customizing-oc-output-with-go-templates)
* [Learn Go Template Syntax (Hashicorp)](https://developer.hashicorp.com/nomad/tutorials/templates/go-template-syntax)
* [Using Go Templates](https://blog.gopheracademy.com/advent-2017/using-go-templates/)


01. Para comenzar, probaremos algunos ejemplos:

    ~~~shell
    {%- raw -%}
    $ kubectl get pods --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}'
    nginx
   
    $ kubectl get pods --template '{{range .items}}{{.metadata.name}}@{{.status.podIP}} <-> {{.spec.nodeName}} {{"\n"}}{{end}}'
    mipod@172.16.87.199 <-> worker-node01
    {% endraw %}
    ~~~


02. Vamos a crear una plantilla algo más compleja.

    Empezaremos buscando qué datos mostrar, p.ej acerca de los nodos.  Para ello obtendremos un listado de información de los nodos en formato JSON.  Mediante `jq keys` nos quedaremos sólo con los descriptores (_keys_) de la información contenida:

    ~~~shell
    $ kubectl get nodes -o json | jq "keys"
    [
      "apiVersion",
      "items",
      "kind",
      "metadata"
    ]

    ~~~


03. ¿ Qué contendrá "items" ?

    ~~~shell
    $ kubectl get nodes -o json | jq ".items" | jq "keys"
    [
      0,
      1,
      2,
      3
    ]
    ~~~


04. Esto es, se trata de un _array_ ("arreglo" o "vector").  Veamos qué contiene, p.ej, el primer elemento del mismo:

    ~~~shell
    $ kubectl get nodes -o json | jq ".items[0]" | jq "keys"
    [
      "apiVersion",
      "kind",
      "metadata",
      "spec",
      "status"
    ]
    ~~~


05. Ahora es el turno de los "metadata":

    ~~~shell
    $ kubectl get nodes -o json | jq ".items[0]" | jq ".metadata" | jq "keys"
    [
      "annotations",
      "creationTimestamp",
      "labels",
      "name",
      "resourceVersion",
      "uid"
    ]
    
    $ kubectl get nodes -o json | jq ".items[0]" | jq ".metadata.name"
    "master-node"
    ~~~


06. Esto ha sido para el primer elemento del _array_.  ¿ Qué ocurrirá si procesamos _todos_ los elementos del vector ?

    ~~~shell
    $ kubectl get nodes -o json | jq ".items[]" | jq ".metadata.name"
    "master-node"
    "worker-node01"
    "worker-node02"
    "worker-node03"
    ~~~

    Con esto podemos concluir que ".items[].metadata.name" puede ser una ruta interesante para nuestra plantilla.


07. Volvamos a explorar qué otros datos mostrar:

    ~~~shell
    $ kubectl get nodes -o json | jq ".items[0]" | jq "keys"
    [
      "apiVersion",
      "kind",
      "metadata",
      "spec",
      "status"
    ]
    ~~~


08. Por analogía con otros objetos usualmente "status" (más que "spec") contiene información interesante:

    ~~~shell
    $ kubectl get nodes -o json | jq ".items[0]" | jq ".status" | jq "keys"
    [
      "addresses",
      "allocatable",
      "capacity",
      "conditions",
      "daemonEndpoints",
      "images",
      "nodeInfo"
    ]

    $ kubectl get nodes -o json | jq ".items[0]" | jq ".status.addresses"
    [
      {
        "address": "10.0.0.10",
        "type": "InternalIP"
      },
      {
        "address": "master-node",
        "type": "Hostname"
      }
    ]
    ~~~


09. Comprobamos para el resto de elementos del _array_:

     ~~~shell
     $ kubectl get nodes -o json | jq ".items[]" | jq ".status.addresses"
     [...]
     ~~~

     Así que esta será la otra información que nos interesa: la dirección IP interna de cada nodo.  Sin embargo, en la ruta ".items[].status.addresses" nos interesa mantener los elementos de tipo "InternalIP" y descartar los de tipo "Hostname"[^1]


10. En resumen, los elementos que nos interesan son:
    * .items[].metadata.name
    * .items[].status.addresses (pero sólo los "InternalIP")

    Así que la plantilla Go que usaremos será:

    ~~~php
    {%- raw -%}
    {{- range .items -}}
        {{ $name := .metadata.name }}
        {{- range .status.addresses -}}
            {{- if eq .type "InternalIP" -}}
                {{$name}} {{":"}} {{ .address }} {{"\n"}}
            {{- end -}}
        {{- end -}}
    {{- end -}}
    {% endraw %}
    ~~~

    Una breve explicación:
    * {% raw %}{{range&nbsp;.items}}...{{end}}{% endraw %}
    : Itera para cada nodo de la lista

    * {% raw %}{{-...-}}{% endraw %}
    : En realidad, los elementos se encierran entre llaves ({% raw %} "{{" {% endraw %}).  Al añadir un guión ("-") en el par de llaves de apertura, o cierre, o ambos, estamos indicando que queremos eliminar espacios superfluos que se puedan generar en la plantilla[^2]
    
    * {% raw %}{{$name:=...}}{% endraw %}
    : Almacena un valor en la variable temporal `name`

    * {% raw %}{{range .status .addresses}}...{{end}}{% endraw %}
    : De nuevo, itera pero esta vez para cada elemento del _array_ de direcciones

    * {% raw %}{{if&nbsp;eq&nbsp;...}}{% endraw %}
    : Condicional, compara si ".status.addresses[].type" es igual a "InternalIP"


11. Por último, guardamos el contenido de la plantilla en un fichero (p.ej, `/tmp/x`) y ejecutamos:

    ~~~shell
    $ nl /tmp/x
    {%- raw -%}
         1  {{- range .items -}}
         2      {{ $name := .metadata.name }}
         3      {{- range .status.addresses -}}
         4          {{- if eq .type "InternalIP" -}}
         5              {{$name}} {{":"}} {{ .address }} {{"\n"}}
         6          {{- end -}}
         7      {{- end -}}
         8  {{- end -}}
    {% endraw %}

    $ kubectl get nodes -o go-template-file /tmp/x
    master-node : 10.0.0.10
    worker-node01 : 10.0.0.11
    worker-node02 : 10.0.0.12
    worker-node03 : 10.0.0.13
    ~~~


<BR>
<BR>
## Y ¿sobre YAML?

Hasta ahora todos los ejemplos han empleado JSON.  Sin embargo, si se prefiere se puede usar YAML prácticamente de la misma manera.

01. Comenzamos instalando [yq](https://mikefarah.gitbook.io/yq/), que nos ayudará a filtar y transformar contenido de manera similar a como hicimos con `jq`:

    ~~~shell
    $ sudo snap install yq
    [...]
    $ which yq
    /snap/bin/yq
    ~~~

02. Probaremos algunos ejemplos con _pods_:

    ~~~shell
    $  kubectl get pods -o yaml | yq "keys"
    - apiVersion
    - items
    - kind
    - metadata

    $ kubectl get pods -o yaml | yq ".items[]"  | yq "keys"
    - apiVersion
    - kind
    - metadata
    - spec
    - status
    ~~~

    {% quiz %}
    ---
    locale: es
    ---
    # ¿ Qué habrá en "metadata" ?
    Dada la salida anterior, ¿ cómo accedería a todos los elementos bajo "metadata", pero sólo del _primer_ elemento del _array_ ? \
    Pista: pulsa en la bombilla
    
    > Siempre puede probar los comandos directamente, y ver cuál **no** falla ;-)
    
    1. [ ] ... | yq ".items.metadata[]"
       > ¿ Seguro que "metadata" es un _array_ ?
    1. [ ] ... | yq ".items[1].metadata"
       > ¿ Seguro que [1] es el primer elemento del _array_ ?
    1. [x] ... | yq ".items[0].metadata"
        > ¡Correcto!
    {% endquiz %}


    Echamos un vistazo a "metadata":

    ~~~shell
    $  kubectl get pods -o yaml | yq ".items[0].metadata"
    annotations:
      cni.projectcalico.org/containerID: b6ffb5f018134be864d28d87601a2bc73f5ec582e6fa127f04f061909b2d8527
      cni.projectcalico.org/podIP: 172.16.87.200/32
      cni.projectcalico.org/podIPs: 172.16.87.200/32
    creationTimestamp: "2023-11-20T06:56:40Z"
    labels:
      app: web
      env: prod
    name: mipod
    namespace: default
    resourceVersion: "49877"
    uid: 5597b7f5-7c30-40e0-87bd-42beae4473ed
    ~~~

    Vista la salida anterior nos quedaremos sólo con los valores "name" y "namespace":

    ~~~shell
    $ kubectl get pods -o yaml | yq '.items[].metadata' | yq '.name, .namespace'
    mipod
    default
    ~~~


03. Otros ejemplos, empleando _nodes_:

    ~~~shell
    $ kubectl get nodes -o yaml | yq "keys"
    - apiVersion
    - items
    - kind
    - metadata

    $ kubectl get nodes -o yaml | yq '.items[0]' | yq "keys"
    - apiVersion
    - kind
    - metadata
    - spec
    - status
    ~~~

    {% quiz %}
    ---
    locale: es
    ---
    # Comando
    Comparando el último comando con el penúltimo ¿ qué es lo que se ha comprobado ?

    
    1. [ ] Es prácticamente el mismo comando; la diferencia es que, en el segundo, al indicar "items[0]" ya no queremos que aparezca en la salida
       > Incorrecto, prueba otra vez.
    1. [ ] En el último comando queremos mostrar los descriptores (_keys_) del _array_ "items"
       > No. Al menos "[0]" ya indica un elemento concreto.
    1. [x] En el último comando queremos mostrar los descriptores (_keys_) del _array_ "items", **pero** como sólo estamos "explorando"nos limitamos al primer elemento del _array_
        > ¡Correcto!
    {% endquiz %}


    Comprobamos qué hay bajo "status":
    
    ~~~shell
    $ kubectl get nodes -o yaml | yq '.items[0].status' | yq 'keys'
    - addresses
    - allocatable
    - capacity
    - conditions
    - daemonEndpoints
    - images
    - nodeInfo
    ~~~


    De ahí, quizás lo que más pueda interesarnos ahora sean las "addresses":

    ~~~shell
    $ kubectl get nodes -o yaml | yq '.items[0].status.addresses[]'
    address: 10.0.0.10
    type: InternalIP
    address: master-node
    type: Hostname

    $ kubectl get nodes -o yaml | yq '.items[].status.addresses[0] | with(.type == "InternaIP"; .address)'
    address: 10.0.0.10
    type: InternalIP
    address: 10.0.0.11
    type: InternalIP
    address: 10.0.0.12
    type: InternalIP
    address: 10.0.0.13
    type: InternalIP
    ~~~



<BR>
<BR>
## Uso del API

Adicionalmente también se puede acceder, de forma programática, a la información por medio del API.  Esto será posible gracias a que en el _Control Plane_ hay ejecutando un _API server_, al cual podemos interrogar.


01. Verificamos, en el nodo _master_, que el _API server_ está operativo:

    ~~~shell
    > ssh vagrant master

    vagrant@master-node:~$ sudo crictl ps --name 'api'
    CONTAINER           IMAGE          CREATED             STATE               NAME                ATTEMPT             POD ID              POD
    20106d6467d72       7fe0e6f[...]   15 hours ago        Running             kube-apiserver      3                   9c5fa3a677d96       kube-apiserver-master-node

    vagrant@master-node:~$ exit
    ~~~


02. Nos conectamos a cualquier nodo.  Desde ahí lanzaremos `kubectl proxy`, que nos abrirá un canal en `localhost:8001` contra el _API server_ del _Control Plane_:

    ~~~shell
    $ kubectl proxy --help
    Creates a proxy server or application-level gateway between localhost
    and the Kubernetes API server. It also allows serving static content
    over specified HTTP path. All incoming data enters through one port
    and gets forwarded to the remote Kubernetes API server port [...]
    ~~~


    Como `kubectl proxy` se queda en espera tendremos que lanzarlo en segundo plano:

    ~~~shell
    $ kubectl proxy &
    [1] 167461
    Starting to serve on 127.0.0.1:8001
    ~~~


03. A partir de aquí ya podemos consultar la información:

    ~~~shell
    $ curl -s http://localhost:8001/api/v1/namespaces/default/pods | jq "."
    [...]

    $ curl -s http://localhost:8001/api/v1/namespaces/default/pods/mipod | jq "."
    [...]
    ~~~


04. En realidad, se puede llegar a mostrar información de todos los elementos de Kubernetes (como por ejemplo la lista de nodos, disponible bajo "http://localhost:8001/api/v1/nodes").

    La mejor forma de comprobarlo es examinar la salida de
    ~~~shell
    $ curl -s http://localhost:8001/api/v1/
    ~~~

    ...e ir explorando.


05. Aprovechando que la variable de entorno `$!` contiene el PID del último comando lanzado en segundo plano, vamos a finalizar el _proxy_:

    ~~~shell
    $ ps -fp $!
    UID          PID    PPID  C STIME TTY          TIME CMD
    vagrant   171598  169201  0 01:29 pts/2    00:00:00 kubectl proxy

    $ kill %%

    $ jobs
    [1]+  Terminated              kubectl proxy

    $ ps -fp $!
    UID          PID    PPID  C STIME TTY          TIME CMD
    ~~~


06. Para finalizar esta sección indicaremos que `kubectl proxy`:
    * puede escuchar en otras IPs y puerto (opciones `--address` y `--port`)
    * puede limitar qué conexiones admite (opciones `--accept-hosts`)
    * puede servir, además del API, ficheros locales (opciones `--www` y `--www-prefix`)
    * puede limitar qué parte del API hace accesible (opciones `--api-prefix`, `--accept-paths` y `--reject-paths`)



<BR>
<BR>
## Limpieza

01. Borramos los objetos creados para este laboratorio:

    ~~~shell
    $ kubectl delete svc/mipod pod/mipod
    service "mipod" deleted
    pod "mipod" deleted

    $ rm -f /tmp/x
    ~~~




# Conclusiones

Hasta ahora habíamos estado usando comandos tales como `kubectl get`, `kubectl get -o wide`, `kubectl describe` para obtener información de los distintos objetos.

Con lo aprendido en este laboratorio hemos podido obtener información de diversas formas: volcando en formato JSON, seleccionando y transformando usando `jq`, o bien usando plantillas Go.  Además se puede emplear YAML de manera equivalente.

También hemos comprobado que se puede, a través de `kube-proxy`, acceder directamente al API de cada objeto.

Un último apunte: para la manipulación de datos en formato JSON hemos empleado `jq`, que es una utilidad cada vez más conocida y usada.  Sin embargo es posible hacer estas operaciones de forma nativa en Kubernetes empleando expresiones mediante [JSONPath](https://kubernetes.io/docs/reference/kubectl/jsonpath/)

<center>— ♠ —</center>



<BR>
<BR>
## Notas a pie de página

[^1]: En realidad _parece_ que los "InternalIP" son el primer elemento del _subarray_, mientras que "Hostname" son el segundo elemento.  Podríamos simplemente habernos quedado con ".items[].status.addresses[0]" ... pero así no hubiésemos aprendido a usar los condicionales :-)

[^2]: Si has usado antes lenguajes como ERB, Jinja2, etc sabrás de qué se está hablando aquí.<BR>Si no los has usado y no entiendes nada no te preocupes: es normal y esperable ;-)  Lo mejor que puedes hacer es repetir la ejecución **con** y **sin** los guiones en las llaves.
