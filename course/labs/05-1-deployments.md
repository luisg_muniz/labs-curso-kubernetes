---
title: 05 Deployments
parent: Laboratorios
---
<details open markdown="block">
  <summary>Sumario</summary>
  {: .text-delta }
1. TOC
{:toc}
</details>



# Objetivo

Hasta ahora hemos estado practicando con _pods_, por lo sencillo que resultan de manejar.  Sin embargo, no son los objetos que típicamente vayamos a manejar (no de forma directa, al menos).

En este laboratorio comenzaremos a usar _deployments_ ("despliegues"), que son objetos mucho más interesantes porque permitirán:
   * monitorizar y relanzar los _pods_ que lo componen,
   * reescalado, tanto manual como automático, y
   * despliegue, y repliegue, de nuevas versiones de los _pods_



# Pasos

## Container > Pod > ReplicaSet > Deployment

01. Creamos un primer deployment usando una imagen sencilla[^1].  Esta vez usaremos una que procede del "Artifactory" de Google (GCR = Google Container Registry):

    ~~~shell
    $ kubectl create deployment "rush" --image k8s.gcr.io/echoserver:1.4 --port 8080
    deployment "rush" created

    $ kubectl get deploy,po
    NAME                   READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/rush   1/1     1            1           50s

    NAME                        READY   STATUS      RESTARTS   AGE
    pod/rush-685b9c6cf9-78qfw   1/1     Running     0          50s
    ~~~


02. El deployment se ha encargado de varias cosas:
    * Ha comprobado cuántas instancias se han solicitado
    * Busca un nodo donde desplegar el pod
      * Lanza (schedules) el pod
      * Monitoriza, y relanza (_re-schedules_) si fuese necesario, el pod


03. Podemos usar `kubectl get pods -o wide` para descubrir en qué nodo se ha lanzado el pod.

    También, podemos conectarnos al nodo en cuestión (`vagrant ssh <máquina>`) y tratar de localizar el contenedor en ejecución; como el _container runtime_ es [CRI-O](https://cri-o.io/), usaremos los comandos `sudo crictl images` y `sudo crictl ps`.


04. Si necesitamos más detalles, podemos usar los subcomandos (verbos) `describe` y `logs`:

    ~~~shell
    $ kubectl get pods
    NAME                  READY     STATUS    RESTARTS   AGE
    rush-f67b467c5-p6wrp   1/1       Running   0          29m

    $ kubectl get pod rush-f67b467c5-p6wrp
    NAME                  READY     STATUS    RESTARTS   AGE
    rush-f67b467c5-p6wrp   1/1       Running   0          29m

    $ kubectl get pod rush-f67b467c5-p6wrp -o wide
    NAME                  READY     STATUS    RESTARTS   AGE   IP            NODE   [...]
    rush-f67b467c5-p6wrp  1/1       Running   0          29m   10.244.0.5    [...]  [...]

    $ kubectl get pod rush-f67b467c5-p6wrp -o wide -o yaml
    [...]

    $ kubectl describe pod rush-f67b467c5-p6wrp
    [...]

    $ kubectl logs rush-f67b467c5-p6wrp
    [...]
    ~~~


    > **Nota**: **No** hay que limitarse a copiar y pegar los nombres del pod ... ¡¡¡ hay que fijarse en la salida de `kubectl get pod` !!!
    

<BR>
<BR>
## Deployment > Service

01. De manera similar a como hizo en anteriores laboratorios, cree un servicio también llamado "rush", de tipo `NodePort`.

02. Compruebe, usando un navegador desde la máquina anfitriona, que es capaz de conectarse al servicio y visualizar el contenido del servidor web del pod.
    Recuerde que debe estar accesible en la IP de cualquier nodo, y a través del puerto (aleatorio) que se le haya asignado.

03. Usando el comando `kubectl edit service/rush` cambie dicho puerto aleatorio por, p.ej, el 30_000.  Compruebe si puede conectarse.


    {% quiz %}
    ---
    locale: es
    ---
    # Forzando las cosas
    Si vuelve a editar el servicio con `kubectl edit ...` y trata de cambiar el puerto desde 30_000 a 9_000:


    1. [ ] El cambio se aplica inmediatamente
       > ¿Seguro que has probado a cambiar `nodePort`?
    1. [ ] El cambio se aplica, pero el navegador **no** conecta
       > ¿Seguro que has cambiado `nodePort`?
    1. [x] El cambio se rechaza
        > ¡Correcto!  Al grabar y salir, el fichero se revisa.  Como el puerto no está en el rango preconfigurado (del 30_000 al 32k) se rechaza.  Volverá a aparecernos el fichero con la configuración, y la cabecera (comentario) de las líneas 1 a 7 nos indicará por qué se ha rechazado.
    {% endquiz %}

<BR>
<BR>
## Plantilla YAML

01. Vamos a crear otro despliegue, esta vez usando un fichero YAML como punto de partida:
    ~~~shell
    ---
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: echo
    spec:
      replicas: 3
      template:
        spec:
          containers:
          - name: echo-pod
            image: k8s.gcr.io/echoserver:1.8
            ports:
            - containerPort: 8080
    ...
    ~~~

    * Nótese que aparecen tanto la directiva `replicas` como la directiva `template`


02. Esa definición también la tiene en <https://pastebin.com/raw/nh0qYTj9>

    Abra esa página y compare las dos definiciones.


03. Cuando haya comprobado que son iguales, lance la creación de este nuevo deployment.  En vez de tener que copiar la definición en un fichero local, aprovecharemos que está definida en esa página web.

    Lanzamos la creación del deployment...

    ~~~shell
    $ kubectl apply -f https://pastebin.com/raw/nh0qYTj9
    The Deployment "echo" is invalid:
    * spec.selector: Required value
    * spec.template.metadata.labels: Invalid value: map[string]string(nil): `selector` does not match template
    ~~~

    ...sólo para comprobar que la definición es errónea :-(

    Faltaba, bajo `spec:`, una nueva directiva llamada `selector`.  Compare el siguiente fragmento de código con los mensajes de error anteriores.  Si nos fijamos, nos daremos cuenta de que el comando `kubectl` indicaba qué elemento faltaba, y qué "estructura" (anidamiento) correspondía:
    ~~~shell
    [...]
    spec:
      replicas: 3
      selector:
        matchLabels:
          app: echoserver
    [...]
    ~~~

    * Además de `matchLabels` es posible usar `matchExpressions`; no son tan sencillas, pero dan mayor potencia (expresividad).  Ejemplo: <https://bit.ly/48XqABv>


04. La definición corregida (que ya incluye `matchLabels`) está disponible en <https://pastebin.com/raw/fuDf619U>.

    Puede abrir esa página en un navegador para comprobarlo, y a continuación intentar crear de nuevo el deployment...

    ~~~shell
    $ kubectl apply -f https://pastebin.com/raw/fuDf619U
    The Deployment "echo" is invalid: spec.template.metadata.labels: Invalid value: map[string]string(nil): `selector` does not match template `labels`
    ~~~

    ...y de nuevo nos vuelven a faltar unas directivas 😅  Se ve que habrá que crear alguna etiqueta (tag).  Viendo el mensaje de error, ¿puede imaginarse qué nivel de anidamiento tendrá en el fichero YAML?

05. El fichero final 😛 es:

    ~~~shell
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: echo
    spec:
      replicas: 3
      selector:
        matchLabels:
          app: echoserver
      template:
        metadata:
          labels:
            app: echoserver
        spec:
          containers:
          - name: echo-pod
            image: k8s.gcr.io/echoserver:1.8
            ports:
            - containerPort: 8080
    ...
    ~~~

    * Esta definición también está en <https://pastebin.com/dvDAftnr>

06. Si compara esta última definición con la anterior (que daba errores) detectará qué es lo que se ha añadido.

    Si compara el último mensaje de error ("spec.template.metadata.labels: Invalid value: map[string]string(nil): `selector` does not match template `labels`") con esas nuevas directivas, ¿le encuentra alguna correlación?

07. El sentido de esa nuevas directivas (`spec.template.metadata.labels`) es que, cuando se cree el despliegue, para la `spec` sólo se usarán aquellas `templates` que se consideren aplicables.  En este caso, que tengan la misma `label` en la `spec` y en el `template`


08. Por último, repetimos la creación del deployment:

    ~~~shell
    Repetimos:
    $ kubectl apply -f https://pastebin.com/raw/dvDAftnr --dry-run=client
    deployment.apps/echo created (dry run)

    $ kubectl apply -f https://pastebin.com/raw/dvDAftnr
    deployment.apps/echo created

    $ kubectl get deploy,pods
    NAME                   READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/echo   3/3     3            3           7s

    NAME                       READY   STATUS    RESTARTS   AGE
    pod/echo-f8b798bc5-j4wpx   1/1     Running   0          7s
    pod/echo-f8b798bc5-rs7h8   1/1     Running   0          7s
    pod/echo-f8b798bc5-wbqkc   1/1     Running   0          7s
    ~~~

    * Y esta vez sí debería funcionar correctamente.

09. Los objetos desplegados los vamos a mantener para el siguiente laboratorio.


# Conclusiones


Complete el siguiente laboratorio [06 Escalado](../06-scale-deployments/) antes de comparar los resultados.


<center>— ♠ —</center>


<BR>
<BR>
## Notas a pie de página

[^1]: La imagen está basada en <https://github.com/jmalloc/echo-server>.  Queda a la escucha en el puerto 8080.
