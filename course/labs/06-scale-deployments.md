---
title: 06 Escalado
parent: Laboratorios
---
<details open markdown="block">
  <summary>Sumario</summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

# Prerequisitos

Antes de iniciar este laboratorio es necesario haber completado el previo, [05 Deployments](../05-1-deployments/)



# Objetivo

Aprovechando los objetos de tipo _deployment_ creados en el anterior laboratorio, vamos a practicar el escalado (horizontal) de las aplicaciones.



# Pasos

## Escalado

01. Probamos a "escalar": añadimos y quitamos, de forma manual, instancias (pods) en ejecución:

    ~~~shell
    $ kubectl scale deploy/echo --replicas=6
    deployment.apps/echo scaled

    $ kubectl get deploy,pods
    NAME                   READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/echo   6/6     6            6           3m11s

    NAME                       READY   STATUS    RESTARTS   AGE
    pod/echo-f8b798bc5-hd257   1/1     Running   0          7s
    pod/echo-f8b798bc5-j4wpx   1/1     Running   0          3m11s
    pod/echo-f8b798bc5-kcrmg   1/1     Running   0          7s
    pod/echo-f8b798bc5-lw2cq   1/1     Running   0          7s
    pod/echo-f8b798bc5-rs7h8   1/1     Running   0          3m11s
    pod/echo-f8b798bc5-wbqkc   1/1     Running   0          3m11s

    $ kubectl scale deploy/echo --replicas=0
    deployment.apps/echo scaled

    $ kubectl get deploy
    NAME                   READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/echo   0/0     0            0           3m59s

    $ kubectl get pods --selector=app=echoserver
    No resources found in default namespace.
    ~~~



<BR>
<BR>
## Autoescalado (horizontal, de pods)

01. Establecemos el criterio de autoescalado:

    ~~~shell
    $ kubectl autoscale deployment/echo --min=2 --max=8 --cpu-percent=60
    horizontalpodautoscaler.autoscaling/echo autoscaled

    $ kubectl get deploy
    NAME   READY   UP-TO-DATE   AVAILABLE   AGE
    echo   0/0     0            0           24m
                                                                                 ✓
    $ kubectl get pods --selector=app=echo
    No resources found in default namespace.
    ~~~

    {% quiz %}
    ---
    locale: es
    ---
    # 0/0, como la cerveza 
    La regla de autoescalado indica que debería haber un mínimo de dos réplicas (`--min=2`).  Entonces, ¿por qué no se han lanzado? >:-/


    1. [ ] El comando `autoscale` en realidad no admite ese `--min=X`
       > Si fuera esa la razón, el comando `kubectl` hubiese dado un error
    1. [ ] El autoescalado sólo funciona en plataformas de pago, como el Cloud, OpenShift, etc.
       > Nops.
    1. [x] Al haber forzado las réplicas a 0, se sobreentiende que se quiere dejar ese despliegue desactivado
        > ¡Correcto!  Se reactivará en cuando las réplicas ya no sean 0
    {% endquiz %}

        

02. Habrá que forzar al menos una réplica para que se entienda ese despliegue como reactivado:

    ~~~shell
    $ kubectl scale deploy/echo --replicas=1
    $ kubectl get deploy,pods
    NAME                   READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/echo   2/2     2            2           9m42s

    NAME                       READY   STATUS    RESTARTS   AGE
    pod/echo-f8b798bc5-b7nh8   1/1     Running   0          3m32s
    pod/echo-f8b798bc5-zglcj   1/1     Running   0          3m32s

    $ kubectl get hpa  ### O bien "horizontalpodautoscaler", que es el nombre real del objeto
    NAME   REFERENCE         TARGETS         MINPODS   MAXPODS   REPLICAS   AGE
    echo   Deployment/echo   <unknown>/60%   2         8         2          27s

    $ kubectl get hpa --watch
    NAME   REFERENCE         TARGETS         MINPODS   MAXPODS   REPLICAS   AGE
    echo   Deployment/echo   <unknown>/60%   2         8         2          4m15s
    [...]
    ~~~

    * Con `hpa --watch`, el comando `kubectl` se queda en espera: si hay cambios en el número de réplicas se mostrarán por pantalla.



03. Abrimos otro terminal.  Simulamos un aumento de carga, lanzando alguna réplica más:

    ~~~shell
    $ kubectl scale deploy/echo --replicas 4
    deployment.apps/echo scaled

    $ kubectl get deploy,pods
    NAME                   READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/echo   4/4     4            4           15m

    NAME                       READY   STATUS    RESTARTS   AGE
    pod/echo-f8b798bc5-b7nh8   1/1     Running   0          9m31s
    pod/echo-f8b798bc5-kqfzw   1/1     Running   0          8s
    pod/echo-f8b798bc5-pwv4h   1/1     Running   0          8s
    pod/echo-f8b798bc5-zglcj   1/1     Running   0          9m31s
    ~~~

    * Pasados unos segundos, en el primer terminal (el del `hpa --watch`) se tiene que mostrar el aumento de réplicas.



04. En este paso tendríamos que lanzar alguna tarea que aumente la carga de los contenedores para poder comprobar si autoescalan correctamente...

    ...el problema es que no  iban a autoescalar

    Con `kubectl describe hpa` debería averigüar por qué no iba a funcionar 😉



05. Finalizamos:

    En el segundo terminal, reescalamos el despliegue a cero (0) pods:

    ~~~shell
    $ kubectl scale deploy/echo --replicas 0
    ~~~

06. En el primer terminal esperamos a que se  refleje el nuevo número de réplicas.  Después, pulsamos [Control+C] para finalizar el comando `kubectl get. hpa --watch`

    En cualquier terminal, eliminamos la regla de autoescalado:

    ~~~shell
    $ kubectl delete hpa echo
    horizontalpodautoscaler.autoscaling "echo" deleted

    $ kubectl get deploy,pods,hpa
    NAME                   READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/echo   0/0     0            0           56m
    ~~~






<center>— ♠ —</center>
