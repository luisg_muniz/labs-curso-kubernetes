---
title: 07 Deployment (examen)
parent: Laboratorios
---
<details open markdown="block">
  <summary>Sumario</summary>
  {: .text-delta }
1. TOC
{:toc}
</details>


# Objetivo

Practicar una pregunta típica de examen.

# Pasos

## Consideraciones previas

En el entorno del examen de certificación [CKA](https://www.cncf.io/training/certification/cka) debemos completar las tareas (de 17 a 20) en el tiempo asignado, _exactamente_ como se nos haya indicado ... pero en realidad no cuenta **cómo** las hayamos hecho.

Por tanto:
* procuraremos no memorizar comandos innecesarios, sino que primaremos saber buscar en la documentación permitida
* procuraremos tener soltura con los comandos y técnicas imprescindibles (así, sólo memorizaremos éstos!)
* usaremos algunos "trucos" para ser más veloces a la hora de completar las distintas tareas.



<BR>
<BR>
## Monitor

Para no estar lanzando innecesariamente las distintas formas del comando `kubectl get` una y otra vez, mejor usaremos un monitor de objetos.

01. Abrimos un terminal y nos conectamos al nodo "node01".  Ahí, lanzamos el comando `watch` para que, cada segundo, nos muestre qué objetos hay creados:

    ~~~shell
    > vagrant ssh node01

    $ watch -n1 kubectl get pods,svc,deploy,rs -o wide
    ~~~



02. Abrimos un segundo terminal y nos conectamos al nodo "node01".  Desde aquí lanzaremos todos los comandos interactivos que necesitemos.

    Comenzamos instalando el paquete necesario para el autocompletado.
    
    ~~~shell
    > vagrant ssh node01
    
    $ sudo apt install -y bash-completion
    ~~~



03. Después, definimos una serie de alias muy útiles, y terminamos de activar el autocompletado:

    ~~~shell
    $ alias k=kubectl c=clear h="history | grep "
    $ source <(kubectl completion bash)
    ~~~

    > **Nota**: Si queremos emplear el autocompletado **también** con el alias "k", en vez de ejecutar el comando anterior habrá que ejecutar la siguiente variante:
    ~~~shell
    $ source <(kubectl completion bash | sed -e '/complete/{p; s/\<kubectl\>/k/}')
    ~~~
    Como el comando es _considerablemente_ más complejo, debe valorar seriamente si lo necesita.  La recomendación es mantener el anterior, mucho más simple.



04. _Cerramos la sesión_ y la volvemos a abrir inmediatamente.  El objetivo es que esos comandos hayan quedado en el histórico:

    ~~~shell
    $ exit
    > vagrant ssh node01
    $ !alias
    $ !source
    ~~~
    
    > **Nota**: A partir de ahora, y para entrenarnos en su uso, en los laboratorios haremos uso **intensivo** de los alias definidos.

    > **Nota**: Aquí ya **hay que teclear TODOS los comandos**, para empezar a memorizar los importantes.



05. Procuraremos lanzar todos los comandos interactivos desde este segundo terminal, ya configurado.  Sin embargo, si fuera necesario abrir otro nuevo terminal, bastará con ejecutar `!alias` y `!source` al entrar para tenerlo configurado.



06. Si se desea, abrimos un tercer terminal y dejamos lanzado el comando `vagrant status`.  Minimizamos inmediatamente porque no es necesario esperar a ver en qué estado están los nodos porque **presuponemos** que están correctamente arranacados.

    Sólo en caso de problemas (p.ej, sospechamos que algún nodo no funciona) o si queremos cerciorarnos de que todos están correctamente arrancados y operativos volveremos a este terminal.   También es el indicado si necesitásemos reiniciar algún nodo.





<BR>
<BR>
## Primer enunciado de examen

   > "Crear un deployment llamado "deploy-example-07" empleando la imagen `nginx:1.25.3-alpine-perl`.  El deployment debe tener tres réplicas."


01. Comenzaremos buscando, con el navegador, cómo crear un deployment.

    Accedemos a <http://docs.k8s.io/>.  En el cuadro de búsqueda introducimos "create deployment".

    > **Nota:** Esta URL (o sus formas alternativas[^1]) hay que conocerla de memoria

    El primer o segundo resultado será [Using kubectl to Create a Deployment \| Kubernetes](https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-intro/).  Al abrir esa URL encontraremos, bajo el epígrafe "Deploy an app", un ejemplo del comando ...

    ~~~shell
    kubectl create deployment kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1
    ~~~

    ...aunque **todavía no lo ejecutaremos**.



02. En su lugar, crearemos una plantilla y la editamos:

    ~~~shell
    $ i=deploy-example-07
    $ k create deploy $i --image nginx:1.25.3-alpine-perl -o yaml --dry-run=client > t07.y
    ~~~

    > **Nota:** Si se puede, use el autocompletado (para `create`, `deployment`). También, dejaremos el nombre del objeto en una variabe (`i`, en este caso).

    > **Nota 2:** Es interesante que los ficheros que vaya generando: (a) se queden en el $HOME, y (b) su nombre tenga relación con la tarea que estamos resolviendo (para evitar confusiones).



03. Editamos la plantilla recién creada:

    ~~~shell
    $ vi t07.y
    [...]
    spec:
      replicas: 1
    [...]
    ~~~

    En este fichero podemos hacer cambios simples.  En nuestro caso, pasamos de una a tres réplicas, tal y como nos piden en el enunciado.



04. Aplicamos los cambios:

    ~~~shell
    $ k apply -f t07.y
    ~~~

    ...y en el terminal que está haciendo de monitor, comprobamos que efectivamente se desplieguen los pods.



05. En realidad, podíamos haber empleado un par de alternativas igualmente válidas:
    * Haber desplegado una primera réplica, y haber escalado a tres usando `k scale deploy/$i --replicas 3`
    * En vez de generar una plantilla básica YAML mediante `--dry-run=client` podíamos haber directamente generado el fichero `t07.y` con una plantilla más completa.<BR>
    Al buscar en la documentación por "deployments", uno de los primeros resultados es [Deployment \| Kubernetes](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/).  De ahí podemos copiar, pegar, y después adaptar, un fichero YAML más completo.



06. Si hubiésemos necesitado cambiar algún otro parámetro que no hubiese sido el básico del número de réplicas, podíamos:
    * Haber empleado `--dry-run=server`, que genera un YAML algo más completo.
    * Haber editado el deployment, una vez creado:
    ~~~shell
    k edit deploy/$i
    ~~~

    Sin embargo, la edición nos permite cambiar sólo algunos campos.

    Hay que tener suficiente preparación (y uso de la documentación) como para poder copiar y pegar un YAML más completo, en caso necesario (por ejemplo, si cambiamos de puertos)



<BR>
<BR>
## Segundo enunciado de examen

   > "Desplegamos una nueva versión de "deploy-example-07", que empleará la imagen `nginx:1.24.0-alpine-slim`.  Documente la razón del cambio"


01. En este caso resulta más fácil editar directamente el deployment:

    ~~~shell
    $ k edit deploy/$i
    [...]
        spec:
          containers:
          - image: nginx:1.25.3-alpine-perl
    [...]
    deployment.apps/deploy-example-07 edited
    ~~~



02. Mientras tanto, observamos en el terminal que hace de monitor:
    * Cómo se crea un "ReplicaSet (rs)" nuevo
    * Cómo se crea una instancia (pod) en el nuevo ReplicaSet.  Cuando finaliza, se destruye una instancia (pod) del ReplicaSet antigüo.
    * El proceso se repite hasta que se han creado todas las instancias nuevas, y se han destruido todas las antigüas.

    El comportamiento descrito se debe a que la "UpdatePolicy" está a su valor por omisión, "RollingUpdate".

    Fíjese en que el "ReplicaSet" antigüo no se borra, sino que queda sin pods activos.  Como en realidad no consume recursos, por precaución se suele dejar un par de días activo (por si fuese necesario "dar marcha atrás"[&nbsp;](https://www.youtube.com/watch?v=fUspLVStPbk) de la actualización) antes de destruirlo.

    Excepcionalmente, vamos a revisar la estrategia de despliegue (y otros detalles):

    ~~~shell
    $ k describe deploy/$i
    Name:                   deploy-example-07
    [...]
    Annotations:            deployment.kubernetes.io/revision: 2
                            kubernetes.io/change-cause: deployed 1.24.0
    Selector:               app=deploy-example-07
    Replicas:               3 desired | 3 updated | 3 total | 3 available | 0 unavailable
    StrategyType:           RollingUpdate
    MinReadySeconds:        0
    RollingUpdateStrategy:  25% max unavailable, 25% max surge
    [...]
    Conditions:
      Type           Status  Reason
      ----           ------  ------
      Available      True    MinimumReplicasAvailable
      Progressing    True    NewReplicaSetAvailable
    OldReplicaSets:  deploy-example-07-6c6c4f6d86 (0/0 replicas created)
    NewReplicaSet:   deploy-example-07-5ddc859984 (3/3 replicas created)
    Events:
      Type    Reason             Age                From                   Message
      ----    ------             ----               ----                   -------
      Normal  ScalingReplicaSet  40m                deployment-controller  Scaled up replica set deploy-example-07-5ddc859984 to 1
      Normal  ScalingReplicaSet  40m (x2 over 63m)  deployment-controller  Scaled down replica set deploy-example-07-6c6c4f6d86 to 2 from 3
    [...]
    ~~~




03. Después, comprobamos cómo ha ido el despliegue:

    ~~~shell
    $ k rollout status deploy/$i
    deployment "deploy-example-07" successfully rolled out

    $ k rollout history deploy/$i
    deployment.apps/deploy-example-07
    REVISION  CHANGE-CAUSE
    1         <none>
    2         <none>
    ~~~



04. Para que quede documentada esta actualización aprovecharemos una característica de Kubernetes que hasta ahora no habíamos empleado: todos los objetos admiten "comentarios" (_annotations_), que son pares de cadenas de texto clave/valor.  Permiten más longitud, y tienen menos restricciones en cuanto a expresiones permitidas, que las "labels".

    En el caso concreto de los deployments, la clave es "kubernetes.io/change-cause":

    ~~~shell
    $ k annotate deploy/$i kubernetes.io/change-cause="deployed 1.24.0"
    deployment.apps/deploy-example-07 annotated

    $ k rollout history deploy/$i
    deployment.apps/deploy-example-07
    REVISION  CHANGE-CAUSE
    1         <none>
    2         deployed 1.24.0
    ~~~



05. Notas
    * Si nos resulta complicado editar con `vi`, siempre podemos especificar, en el propio comando de edición, qué editor usar:
    ~~~shell
    EDITOR=nano k edit deploy/$i
    ~~~
    * Para la actualización también podíamos haber usado el comando:
    ~~~shell
    k set deploy/$i image nginx:1.24.0-alpine-slim
    k annotate deploy/$i kubernetes.io/...
    ~~~
    * ...o bien haber exportado a fichero YAML, modificarlo, y haber vuelto a aplicar mediante:
    ~~~shell
    k apply -f t07b.y
    ~~~



<BR>
<BR>
## Tercer enunciado de examen

   > "Vuelva a desplegar la versión original.  De nuevo, documente la razón del cambio"


01. Comprobamos el histórico de despliegues:

    ~~~shell
    $ k rollout history deploy/$i
    deployment.apps/deploy-example-07
    REVISION  CHANGE-CAUSE
    1         <none>
    2         deployed 1.24.0

    $ k rollout history deploy/$i --revision 2
    deployment.apps/deploy-example-07 with revision #2
    Pod Template:
      Labels:       app=deploy-example-07
            pod-template-hash=5ddc859984
      Annotations:  kubernetes.io/change-cause: deployed 1.24.0
      Containers:
       nginx:
        Image:      nginx:1.24.0-alpine-slim
        Port:       <none>
        Host Port:  <none>
        Environment:        <none>
        Mounts:     <none>
      Volumes:      <none>
    ~~~


02. Deshacemos el último despliegue:

    ~~~shell
    $ k rollout undo deploy/$i ### Opcional, --to-revision=N
    deployment.apps/deploy-example-07 rolled back
    ~~~


    {% quiz %}
    ---
    locale: es
    ---
    # Rollout interruptus

    El paso a la revisión anterior, ¿ ha tardado más o menos que la actualización en primera instancia ?
    1. [ ] Ha tardado **más**
       > ¿ Segur@ que no es una sensación subjetiva ? De todas formas, no es lo que se _supone_ que tenía que ocurrir.  Prueba otra respuesta.
    1. [ ] Ha tardado **prácticamente igual**
       > ¿ Segur@ que no es una sensación subjetiva ? De todas formas, no es lo que se _supone_ que tenía que ocurrir.  Prueba otra respuesta.
    1. [x] Ha tardado **menos**
        > ¡Correcto!  Al menos, es lo que se espera que ocurra.  A ello contribuye que el ReplicaSet ya está creado y, sobre todo, que las imágenes necesarias ya están descargadas.
    {% endquiz %}



03. Y comentamos:

    ~~~shell
    $ k annotate deploy/$i kubernetes.io/change-cause="rolled back to 1.25.3"
    deployment.apps/deploy-example-07 annotated

    $ k rollout history deploy/$i
    deployment.apps/deploy-example-07
    REVISION  CHANGE-CAUSE
    2         deployed 1.24.0
    3         rolled back to 1.25.3
    ~~~



<BR>
<BR>
## Limpieza

01. Destruimos los objetos creados en este laboratorio, y cerramos la sesión de este terminal "interactivo":

    ~~~shell
    $ k delete deploy/$i
    deployment.apps "deploy-example-07" deleted
    $ exit
    ~~~


02. Revisamos, en el terminal usado como monitor, que han desaparecido tanto el deployment como los replica sets y los pods.

    Sin embargo, no es necesario que cerremos este otro terminal "monitor".



# Conclusiones

Por un lado, hemos empezado a realizar tareas de una forma más rápida en previsión del posible examen de certificación.  Y cómo preparar la sesión el día del examen.

Por otro hemos practicado los despliegues en su forma más básica.

Si mira la ayuda del comando `kubectl rollout` comprobará que existen dos subcomandos ("verbos") que no hemos empleado: `pause` y `resume`.  ¿ Puede imaginar en qué caso pueden ser _realmente_ útiles ? 🤔

<center>— ♠ —</center>



<BR>
<BR>
## Notas a pie de página

[^1]: <http://k8s.io/docs>, <http://docs.k8s.io>, <http://kubernetes.io/docs> y <http://docs.kubernetes.io> acaban llevando a la misma URL

