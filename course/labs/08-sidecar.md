---
title: 08 Sidecar
parent: Laboratorios
---
<details open markdown="block">
  <summary>Sumario</summary>
  {: .text-delta }
1. TOC
{:toc}
</details>

# Prerequisitos

Se presupone que se ha realizado la preparación de la máquina descrita en la sección "Monitor" del laboratorio [07 Deployment (examen)](../07-examen-deployment/#monitor)




# Objetivo



# Pasos

## Preparación

01. Abrimos el terminal que contiene el "monitor".
    Si lo hemos cerrado, basta abrir una sesión en "node01" y lanzar el siguiente comando:

    ~~~shell
    > vagrant ssh node01

    $ watch -n1 kubectl get pods,svc,deploy,rs -o wide
    ~~~



02. Abrimos el terminal "interactivo".

    Tal y como se describió en el [laboratorio anterior](../07-examen-deployment/), debe bastar con recuperar la configuración del histórico de comandos:

    ~~~shell
    > vagrant ssh node01

    $ !alias
    $ !source
    ~~~
    

## Primer enunciado de examen

   > "Crear un pod llamado 'example-08' que contenga dos contenedores.  Uno, llamado 'web', basado en la imagen 'nginx', mientras que el otro estará basado en una imagen 'redis'.


01. De nuevo, la opción más rápida será generar un fichero YAML, y modificarlo para generar a partir de ahí el objeto.

    Si buscamos, en la documentación de Kubernetes, por las palabras "reference kubectl" llegaremos a la [Kubectl Reference Docs](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands).  Ahí, en el índice de la parte izquierda podemos bajar hasta la sección [run](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#run).  Y usaremos cualquiera de los ejemplos que aparecen en la parte derecha.

    ~~~shell
    $ i=example-08
    $ k run $i --image nginx -o yaml --dry-run=client > t08.y
    ~~~

    Nótese que `kubectl run` sólo permite dar de alta un pod con una única imagen.



02. Editamos el fichero de configuración.  No necesitamos las líneas 12 a 15, y las podemos eliminar:

    ~~~shell
    12     resources: {}
    13   dnsPolicy: ClusterFirst
    14   restartPolicy: Always
    15 status: {}
    ~~~


03. El primer contenedor queda definido en las líneas 10 y 11.  Habrá que renombrar "nginx" a "web", tal y como pide el enunciado:

    ~~~shell
    10   - image: nginx
    11     name: web
    ~~~    


04. Para el otro contenedor, basta con replicar las líneas 10 y 11:

    ~~~shell
    12   - image: redis
    13     name: redis
    ~~~


05. Grabamos y desplegamos:

    ~~~shell
    $ k apply -f t08.y
    $ k annotate pod/$i description="Sidecar container"
    pod/example-08 annotated
    ~~~

    En el terminal usado como monitor debemos verificar que se está creando el pod.  Además, la columna "READY" debe indicar "0/2" e ir progresando hasta "2/2".  También, deberán aparecer los dos pod en cuestión.



06. La tarea ya está completada.  Ahora, simplemente por curiosidad vamos a revisar que todo está correcto:

    ~~~shell
    $ k describe pod/$i
    [...]

    $ k logs pod/$i -c redis
    [...]
    
    $ k logs pod/$i -c web
    [...]
    ~~~

## Repaso

   De hecho, este tipo de configuraciones son muy útiles y hasta tienen un nombre específico: [Sidecar Containers](https://kubernetes.io/docs/concepts/workloads/pods/#how-pods-manage-multiple-containers)

   Merece la pena comentar que los dos pod pueden comunicarse entre sí (a través de la IP 127.0.0.1).  Y ambos tienen acceso a la IP exclusiva del pod.

   Todo ello se describe en la [documentación](https://kubernetes.io/docs/concepts/workloads/pods/#pod-networking)



<BR>
<BR>
## Segundo enunciado de examen

   > "Crear otro pod llamado 'trouble-08' que también contenga dos contenedores.  Uno, basado en la imagen 'nginx', mientras que el otro estará basado en una imagen 'varnish'.

01. Podríamos partir del fichero YAML que hemos usado para el anterior pod.  Sin embargo, vamos a reaprovechar el comando "run" y usar el alias `h` que habíamos definido en el laboratorio anterior:

    ~~~shell
    $ h run
    [...]
    450  k run $i --image nginx -o yaml --dry-run=client > t08.y
    483  h run
    ~~~

    De todas las líneas del histórico que contienen la palabra "run", la que nos vale es la penúltima (comando 450).  Volvemos a ejecutarlo:

    ~~~shell
    $ mv t08.y old-t08.y
    
    $ i=trouble-08

    $ !450
    k run example-08 --image nginx -o yaml --dry-run=client > t08.y
    
    $ nl t08.y
    apiVersion: v1
    kind: Pod
    metadata:
      creationTimestamp: null
      labels:
        run: trouble-08
      name: trouble-08
    spec:
      containers:
      - image: nginx
        name: trouble-08
        resources: {}
      dnsPolicy: ClusterFirst
      restartPolicy: Always
    status: {}
    ~~~


02. Al igual que en el caso anterior habrá que modificar el fichero antes de crear el pod:

    ~~~shell
    $ vi t08.y
    [...]

    $ nl t08.y
         1  apiVersion: v1
         2  kind: Pod
         3  metadata:
         4    creationTimestamp: null
         5    labels:
         6      run: trouble-08
         7    name: trouble-08
         8  spec:
         9    containers:
        10    - image: nginx
        11      name: nginx
        12    - image: varnish
        13      name: varnish
    ~~~



03. Creamos el pod:

    ~~~shell
    $ h apply
      399  [...]
      455  k apply -f t07.y
      469  k apply -f t08.y
      495  h apply

    $ !469
    k apply -f t08.y
    pod/trouble-08 created
    ~~~


    {% quiz %}
    ---
    locale: es
    ---
    # ¡Problemas!

    Si se fija en el monitor esta vez comprobará que **no** se crea el pod.<BR>
    Consultando los logs de cada contenedor, ¿puede descubrir por qué?
    1. [ ] El contenedor "varnish" no tiene memoria suficiente
       > Consulte los logs
    1. [ ] El contenedor "nginx" da error porque ya existe otro nombrado igual
       > Los dos pods son independientes entre sí.  Consulte los logs
    1. [ ] El contenedor "nginx" da error porque ya existe otro contenedor "nginx" en el otro pod, que monopoliza sus mismos recursos (puertos, ficheros, bibliotecas de código, etc).
       > Los dos pods son independientes entre sí.  Consulte los logs    
    1. [x] El contenedor "varnish" se encuentra con el puerto 80 ocupado
        > ¡Correcto!  
    {% endquiz %}

04.  Efectivamente, es un problema debido a que los dos contenedores compiten por un mismo puertos: 80/TCP

     Mientras que "nginx" arranca sin mayor problema:
     ~~~shell
     $ k logs pod/$i -c nginx
     /docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
     /docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
     /docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
     10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
     10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
     /docker-entrypoint.sh: Sourcing /docker-entrypoint.d/15-local-resolvers.envsh
     /docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
     /docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
     /docker-entrypoint.sh: Configuration complete; ready for start up
     2023/11/28 07:15:03 [notice] 1#1: using the "epoll" event method
     2023/11/28 07:15:03 [notice] 1#1: nginx/1.25.3
     2023/11/28 07:15:03 [notice] 1#1: built by gcc 12.2.0 (Debian 12.2.0-14)
     2023/11/28 07:15:03 [notice] 1#1: OS: Linux 5.15.0-83-generic
     2023/11/28 07:15:03 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1048576:1048576
     2023/11/28 07:15:03 [notice] 1#1: start worker processes
     ~~~

     ...en el caso de "varnish" se verá el error:
     ~~~shell
     $  ^nginx^varnish
     k logs pod/$i -c varnish
     Error: Could not get socket :80: Permission denied
     (-? gives usage)
     ~~~

     > **Nota**: La notación `^cad1^cad2` significa: "en el comando anterior ..." (de ahí el símbolo `^`, que es una "flecha" apuntando hacia arriba" 😅) "...cambiamos (la primera vez que aparece) 'cad1' por 'cad2', y ¡a ejecutar!"

     No hace falta que resolvamos el problema, simplemente basta con haberlo detectado.



## Limpieza



01. Destruimos los objetos creados en este laboratorio, y cerramos la sesión de este terminal “interactivo”:

    ~~~shell
    $ k delete pod/example-08 pod/trouble-08 --force
    Warning: Immediate deletion does not wait for confirmation that the running resource has been terminated. The resource may continue to run on the cluster indefinitely.
    pod "example-08" force deleted
    pod "trouble-08" force deleted

    $ exit
    ~~~

> **Nota**: ¿ Cuál cree que es la razón *real* para que, en esta ocasión, hayamos proporcionado el flag `--force` ?
    
02. Revisamos, en el terminal usado como monitor, que han desaparecido los dos pods y los contenedores asociados.

    Sin embargo, no es necesario que cerremos este otro terminal “monitor”.



# Conclusiones

Hemos visto como crear un pod de tipo "Sidecar".  Plantéese en qué situaciones puede ser interesante utilizar este patrón de diseño.

De forma similar, hay otro patrón muy parecido llamado [Init Containers](https://kubernetes.io/docs/concepts/workloads/pods/init-containers/)

Por último, ha podido comprobar un problema que surge con los patrones de tipo "Sidecar".  ¿ Cuáles cree que hubieran sido buenas alternativas para resolver este problema ?


<center>— ♠ —</center>
