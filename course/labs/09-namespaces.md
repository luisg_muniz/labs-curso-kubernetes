---
title: 09 Namespaces
parent: Laboratorios
---
<details open markdown="block">
  <summary>Sumario</summary>
  {: .text-delta }
1. TOC
{:toc}
</details>


# Prerequisitos

Se recomienda tener dos terminales abiertos, uno como monitor y otro como terminal interactivo, tal y como se describía en la sección "Monitor" del laboratorio [07 Deployment (examen)](../07-examen-deployment/#monitor)


# Objetivo



# Pasos

## Revisión de los namespaces existentes

01. Comprobamos qué _namespaces_ (ns) hay ya definidos:

    ~~~shell
    $ kubectl get ns -o wide
    NAME                   STATUS   AGE
    default                Active   11d
    kube-node-lease        Active   11d
    kube-public            Active   11d
    kube-system            Active   11d
    kubernetes-dashboard   Active   11d
    ~~~

    Recordemos que:
    * kube-system
    : Para objetos del propio sistema Kubernetes
    * kube-public
    : Para recursos que deben ser visibles por cualquiera (p.ej, información del cluster)
    * kube-node-lease
    : Almacena los _node heartbeats_: se tratan como _leases_ para que se tengan que renovar y así se comprueba si los nodos siguen estando operativos


02. Sobre los objetos en cada uno de ellos:

    ~~~shell
    $ kubectl get all -n kube-public
    No resources found in kube-public namespace.

    $ kubectl get all -n kube-system
    NAME                                           READY   STATUS    RESTARTS         AGE
    pod/calico-kube-controllers-74d5f9d7bb-5rftn   1/1     Running   5                11d
    pod/calico-node-2hbcq                          1/1     Running   5                11d
    [...]
    replicaset.apps/coredns-5dd5756b68                   2         2         2       11d
    replicaset.apps/metrics-server-69fb86cf66            1         1         1       11d
    ~~~


04. También podemos hacer la búsqueda inversa, esto es, por tipo de objeto pero sin importar en qué namespace se encuentren:

    ~~~shell
    $ kubectl get deploy -A  ### O bien "--all-namespaces"
    NAMESPACE              NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
    kube-system            calico-kube-controllers     1/1     1            1           11d
    kube-system            coredns                     2/2     2            2           11d
    kube-system            metrics-server              1/1     1            1           11d
    kubernetes-dashboard   dashboard-metrics-scraper   1/1     1            1           11d
    kubernetes-dashboard   kubernetes-dashboard        1/1     1            1           11d
    ~~~

    Cabe recordar que, en principio, `kubectl get ...` sólo muestra los objetos que estén en el namespace _default_.


04. No todos los tipos de objetos pueden separarse por namespaces:

    ~~~shell
    $ kubectl api-resources --namespaced=true
    NAME                        SHORTNAMES   APIVERSION                     NAMESPACED   KIND
    bindings                                 v1                             true         Binding
    configmaps                  cm           v1                             true         ConfigMap
    [...]
    roles                                    rbac.authorization.k8s.io/v1   true         Role
    csistoragecapacities                     storage.k8s.io/v1              true         CSIStorageCapacity
    ~~~



## Nuevos namespaces

01. Comenzaremos creando los nuevos namespaces, tanto por comando...

    ~~~shell
    $ kubectl create ns zipi
    namespace/zipi created

    $ kubectl get ns
    [...]
    zipi                   Active   6m34s

    $ kubectl describe ns zipi
    Name:         zipi
    Labels:       kubernetes.io/metadata.name=zipi
    Annotations:  <none>
    Status:       Active

    No resource quota.

    No LimitRange resource.
    ~~~


    ...como por fichero:

    ~~~shell
    $ curl -sL https://pastebin.com/raw/Z04nqvgY | nl
         1  apiVersion: v1
         2  kind: Namespace
         3  metadata:
         4    name: zape
         5    labels:
         6      foo: bar

    $ kubectl apply -f https://pastebin.com/raw/Z04nqvgY
    namespace/zape created

    $ kubectl describe ns zape
    Name:         zape
    Labels:       foo=bar
                  kubernetes.io/metadata.name=zape
    Annotations:  <none>
    Status:       Active

    No resource quota.

    No LimitRange resource.
    ~~~


## Despliegues en namespaces

01. Creamos un primer pod en el namespace "zipi":

    ~~~shell
    $ kubectl run 1-nginx --image nginx --namespace zipi
    pod/1-nginx created

    $ kubectl get pods
    No resources found in default namespace.

    $ kubectl get all -n zipi
    NAME          READY   STATUS    RESTARTS   AGE
    pod/1-nginx   1/1     Running   0          12s
    ~~~


02. Antes de crear objetos en "zape", vamos a aplicarle una "quota":

    ~~~shell
    $ curl -sL https://pastebin.com/raw/mxLFmcMk | nl
         1  apiVersion: v1
         2  kind: ResourceQuota
         3  metadata:
         4    name: quota-2g2cpu
         5    ### namespace: zape      ### NOOOORL => https://imgflip.com/i/81h7z8
         6  spec:
         7    hard:
         8      limits.cpu: "2"        ### Limits   => lo máximo que se me "permite"
         9      limits.memory: 2Gi
        10      requests.cpu: "1"      ### Requests => lo que se me "garantiza"
        11      requests.memory: 128Mi
        12      pods: "3"
        13
    $
    $ kubectl apply -f https://pastebin.com/raw/mxLFmcMk -n zape
    resourcequota/quota-2g2cpu created
    ~~~


03. Comprobamos:

    ~~~shell
    $ kubectl describe ns zape
    Name:         zape
    Labels:       foo=bar
                  kubernetes.io/metadata.name=zape
    Annotations:  <none>
    Status:       Active

    Resource Quotas
      Name:            quota-2g2cpu
      Resource         Used  Hard
      --------         ---   ---
      limits.cpu       0     2
      limits.memory    0     2Gi
      pods             0     3
      requests.cpu     0     1
      requests.memory  0     128Mi

    No LimitRange resource.
    ~~~


03. Creamos ahora un deployment sobre el namespace "zape":

    ~~~shell
    $ kubectl create deployment 2-nginx --image nginx --replicas 3 --namespace zape
    deployment.apps/2-nginx created
    ~~~

    Sin embargo, al verificar nos llevaremos una pequeña sorpresa 😮:

    ~~~shell
    NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/2-nginx   0/3     0            0           7m47s

    NAME                                 DESIRED   CURRENT   READY   AGE
    replicaset.apps/2-nginx-68f8f74bfb   3         0         0       7m47s
    ~~~


    {% quiz %}
    ---
    locale: es
    ---
    # ¿Y este error?
    ¿Por qué cree que se ha producido el error?
    
    1. [ ] Hemos especificado tres réplicas, con lo que se supera el número de pods admitidos en el namespace
       > No exactamente; hay mensaje(s) que indican otra causa
    1. [ ] Sólo se trata de la imagen, que tarda en descargar
       > No, hay mensaje(s) que indican otra causa
    1. [ ] No hay recursos suficientes
       > No exactamente; tiene que ver con la quota recién instaurada, pero no es exactamente por falta de recursos
    1. [x] La definición del deployment está incompleta
        > ¡Correcto! Al haber especificado `request`/`limits` de CPU y de memoria, estamos obligados a indicar cuántos recursos de cada necesitará nuestro deployment.
    {% endquiz %}


04. Comprobamos la causa del error:

~~~shell
$ kubectl describe rs/2-nginx-68f8f74bfb -n zape
Name:           2-nginx-68f8f74bfb
Namespace:      zape
[...]
Events:
  Type     Reason        Age    From                   Message
  ----     ------        ----   ----                   -------
  Warning  FailedCreate  55m    replicaset-controller  Error creating: pods "2-nginx-68f8f74bfb-c8hk9" is forbidden: failed quota: quota-2g2cpu: must specify limits.cpu for: nginx; limits.memory for: nginx; requests.cpu for: nginx; requests.memory for: nginx
~~~


05. Vamos a reintentar el despliegue.
    Para ello **podría**:
    * eliminar el deployment erróneo
    * crear un YAML con la definición del despliegue
    * añadir, en el YAML, la definición de los recursos
    * volver a intentar el despliegue

    (y todo ello, no necesariamente en ese orden).

    Para la definición de los recursos puede emplear:

    ~~~yaml
    [...]
      containers:
      - name: example-container
        image: nginx:latest
        resources:
          limits:
            cpu: "1" #  1 CPU core = 1000m(ilicores)
            memory: "256Mi"
          requests:
            cpu: "0.5" # 0.5 CPU cores = 500m(ilicores)
            memory: "180Mi"
     [...]
     ~~~

     O bien fijarse en [un ejemplo](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#example-1)


<BR>
<BR>
## Limpieza

01. Compruebe que, al borrar un namespace, **se borran todos los objetos que contiene**:

    ~~~shell
    $ kubectl delete ns/zipi ns/zape
    ...
    ~~~

# Conclusiones



<center>— ♠ —</center>

