---
title: 10 API
parent: Laboratorios
---
<details open markdown="block">
  <summary>Sumario</summary>
  {: .text-delta }
1. TOC
{:toc}
</details>


# Objetivo

En este laboratorio vamos a explorar el API REST empleado por Kubernetes.  Aprenderemos a extraer las credenciales que está usando `kubectl` para interactúar con el _Control Plane_.  Y con esas credenciales vamos a poder interrogar directamente al API.


# Pasos


## Comando kubectl

Como ya sabíamos por la arquitectura de Kubernetes, el comando `kubectl` interactúa con el `kube-apiserver`: en esencia, por medio de [métodos HTTP](https://es.wikipedia.org/wiki/Protocolo_de_transferencia_de_hipertexto#M%C3%A9todos_de_petici%C3%B3n) (GET, POST, DELETE) hace peticiones al [API REST](https://es.wikipedia.org/wiki/Transferencia_de_Estado_Representacional).  Para la [serialización](https://es.wikipedia.org/wiki/Serializaci%C3%B3n) de datos se emplea [JSON](https://es.wikipedia.org/wiki/JSON)[^1][^2]

Sorprendentemente, resulta que `kubectl` utiliza internamente `curl`. Podemos comprobarlo:

01. Hacemos cualquier tipo de consulta simple:

    ~~~shell
    $ kubectl get ns
    NAME                   STATUS   AGE
    default                Active   28h
    kube-node-lease        Active   28h
    kube-public            Active   28h
    kube-system            Active   28h
    kubernetes-dashboard   Active   28h
    ~~~


02. `kubectl` admite un nivel de detalle (_verbosity_) que va de 1 a 10:

    ~~~shell
    $ kubectl --v=7 get ns 2>&1 | nl
         1  I0529 22:07:03.137878  113840 loader.go:373] Config loaded from file:  /home/kreator/.kube/config
         2  I0529 22:07:03.141539  113840 round_trippers.go:463] GET https://10.0.0.10:6443/api/v1/namespaces?limit=500
         3  I0529 22:07:03.141550  113840 round_trippers.go:469] Request Headers:
         4  I0529 22:07:03.141557  113840 round_trippers.go:473]     Accept: application/json;as=Table;v=v1;g=meta.k8s.io,application/json;as=Table;v=v1beta1;g=meta.k8s.io,application/json
         5  I0529 22:07:03.141563  113840 round_trippers.go:473]     User-Agent: kubectl/v1.27.14 (linux/amd64) kubernetes/f678fbc
         6  I0529 22:07:59.942788  113840 round_trippers.go:574] Response Status: 200 OK in 8 milliseconds
         7  NAME                   STATUS   AGE
         8  default                Active   28h
         9  kube-node-lease        Active   28h
        10  kube-public            Active   28h
        11  kube-system            Active   28h
        12  kubernetes-dashboard   Active   28h
    ~~~


   * Podemos comprobar que:
     * Se toma la configuración de `.kube/config`, bajo el _home_ del usuario/a.
     * Se hace una petición HTTP GET (línea 2), serializada mediante JSON (línea 4)
     * Se ha identificado como `User-Agent` "kubectl" (línea 5), aún a pesar de usar CURL (esto se comprobará en el siguiente paso).
     * Se ha aceptado la petición (HTTP 200 OK, línea 6)


03. Si incrementamos el nivel de detalle hasta 10 podremos comprobar aún más detalles:

    ~~~shell
    $ kubectl --v=10 get ns 2>&1 | nl
         1  I0529 22:07:03.141564  115391 loader.go:373] Config loaded from file:  /home/kreator/.kube/config
         2  I0529 22:07:03.141547  115391 cached_discovery.go:113] returning cached discovery info from /home/kreator/.kube/cache/discovery/10.0.0.10_6443/servergroups.json
         [...]
         77  I0529 22:07:03.141574  115391 round_trippers.go:466] curl -v -XGET  -H "Accept: application/json;as=Table;v=v1;g=meta.k8s.io,application/json;as=Table;v=v1beta1;g=meta.k8s.io,application/json" -H "User-Agent: kubectl/v1.27.14 (linux/amd64) kubernetes/f678fbc" 'https://10.0.0.10:6443/api/v1/namespaces?limit=500'
         [...]
         88  I0529 22:07:03.141580  115391 request.go:1188] Response Body: {"kind":"Table","apiVersion":"meta.k8s.io/v1", [...]
    ~~~

    * Podemos comprobar que:
      * El directorio `.kube/cache/` hace de caché para ciertos datos proporcionados por el servidor (línea 2).
      * Realmente se está usando el comando `curl` (en, o cerca de, la línea 77).
      * También en esa línea se pueden comprobar qué cabeceras se están pasando el la petición HTTP al API REST (opción `-H` de `curl`).
      * Asimismo se puede comprobar qué método se usó (opción `-X` de `curl`)
      * Hacia el final de la salida (línea 88 en este caso concreto) tenemos los datos "en crudo" de la respuesta.


    {% quiz %}
    ---
    locale: es
    ---
    # Caché, o no caché ... he ahí el dilema
    La información obtenida (listado de _namespaces_), ¿será almacenada en la caché `.kube/cache/`? \
    Pista: pulsa en la bombilla

    > Localiza, en la salida de `kubectl --v=10 ...`, las "Response Headers"

    1. [ ] Sí
       > Prueba otra vez
    1. [x] No
       > Correcto; una de las "Response Headers" hace referencia a la directiva de caché ('Cache-Control: no-cache, private').
    1. [ ] Ahora no, [Paco](https://media.licdn.com/dms/image/D4D22AQHmzvH05GjUmw/feedshare-shrink_2048_1536/0/1704028680272?e=2147483647&v=beta&t=AGjV0izL22yyKzPAItpS4yynOXW0ATpVIc9cPPXN5Lk)
    {% endquiz %}


## Consulta (sin autenticar) del API

Podemos realizar directamente consultas a `kube-apiserver` por medio del API REST.  En el siguiente ejemplo, usaremos para ello `curl`...

~~~shell
$ curl -XGET  https://10.0.0.10:6443/api/v1/namespaces
curl: (60) SSL certificate problem: unable to get local issuer certificate
More details here: https://curl.se/docs/sslcerts.html

curl failed to verify the legitimacy of the server and therefore could not
establish a secure connection to it. To learn more about this situation and
~~~

...pero, lamentablemente, no estamos autenticados y por tanto tampoco tenemos autorización para consultar esos datos[^3].


## Consulta (con autenticación) del API

Vamos a extraer los datos de autenticación directamente del fichero `.kube/config`, y con ellos operaremos empleando `curl`.

El procedimiento que vamos a usar implica el uso de muchos comandos estándar Unix/Linux.  Sin embargo, al tratarse de un curso es _posible_ que nuestro cluster Kubernetes esté [implementado](https://github.com/techiescamp/vagrant-kubeadm-kubernetes) sobre una plataforma (máquina) Windows :-(

¡Pero no todo está perdido!  Vamos a aprovechar que:
* los propios nodos "node01" y "node02" son Linux, y deberían tener las herramientas que necesitamos.
*  la implementación hace que el directorio base de `vagrant-kubeadm-kubernetes` esté accesible en los nodos bajo el punto de montaje `/vagrant`.  Así pues, tendremos acceso a la plantilla del fichero de configuración.

Los pasos son:
01. Nos conectamos a uno de los nodos, p.ej "node01" y verificamos que llegamos a los datos originales:

    ~~~shell
    Welcome to Ubuntu 22.04.3 LTS (GNU/Linux 5.15.0-83-generic x86_64)
    [...]
    vagrant@node01:~$ ls /vagrant
    CHANGELOG  configs  LICENSE.md  README.md  scripts  settings.yaml  Vagrantfile
    vagrant@node01:~$ F=/vagrant/configs/config
    vagrant@node01:~$ file $F
    /vagrant/configs/config: ASCII text, with very long lines (2257)
    vagrant@node01:~$ mkdir /vagrant/datos-lab
    vagrant@node01:~$ cd /vagrant/datos-lab
    vagrant@node01:/vagrant/datos-lab$
    ~~~


02. Comprobamos que el fichero contiene los datos necesarios y también están instaladas las herramientas que usaremos:

    ~~~shell
    ...$ cat $F | egrep "client-|cert|server"
    [...]
    ...$ which awk base64 jq
    /usr/bin/awk
    /usr/bin/base64
    /usr/bin/jq
    ~~~


03. Extraemos los certificados necesarios.  Antes comprobaros si podemos obtener esos datos del fichero:

    ~~~shell
    ...$ cat $F | awk '/client-cert.+-data/'
        client-certificate-data: LS0tLS[...]
    ~~~


04. Como sí accedemos, completamos el comando anterior y ejecutamos:

    ~~~shell
    ...$ cat $F | awk '/client-cert.+-data/ {print $2}' | base64 -d -| tee ./client.pem | nl
     1  -----BEGIN CERTIFICATE-----
     2  MIIDKTCCAhGgAwIBAgIINmK4+l5ld1YwDQYJKoZIhvcNAQELBQAwFTETMBEGA1UE
     [...]
    19  -----END CERTIFICATE-----
    ~~~


05. Repetimos para el resto de claves necesarias:


    ~~~shell
    ...$ cat $F | awk '/client-key-data/ {print $2}' | base64 -d | tee ./key.pem | nl

    ...$ cat $F | awk '/cert.+-auth.+/ {print $2}'| base64 -d | tee ./ca.pem | nl

    ...$ file ./*pem
    ca.pem:     PEM certificate
    client.pem: PEM certificate
    key.pem:    PEM RSA private key
    ~~~


06. Finalmente, hacemos la petición:

    ~~~shell
    ...$ curl -s --cert ./client.pem --key ./key.pem --cacert ./ca.pem https://10.0.0.10:6443/api/v1/namespaces | less
    ~~~

    * Hemos obtenido un volcado JSON muy largo pero, en realidad, los datos solicitados (listado de _namespaces_) **están ahí**.  Para poder extraerlos con facilidad tendremos que emplear la herramienta `jq`.


07. Para no estar constantemente interrogando a `kube-apiserver` vamos a guardar en caché el volcado JSON:

    ~~~shell
    ...$ !! | tee ns.json | wc -l
    curl -s --cert ./client.pem --key ./key.pem --cacert ./ca.pem https://10.0.0.10:6443/api/v1/namespaces | less > ns.json

    ...$ I=ns.json
    ~~~


08. Filtramos:

    ~~~shell
    ...$ cat $I | jq "keys"
    [
      "apiVersion",
      "items",
      "kind",
      "metadata"
    ]
    ~~~


09. Vamos extrayendo más información:

    ~~~shell
    ...$ cat $I | jq ".apiVersion"
    "v1"

    ...$ cat $I | jq ".kind"
    "NamespaceList"

    ...$ cat $I | jq ".metadata"
    {
      "resourceVersion": "72582"
    }
    ~~~

    * Por cierto, ese `resourceVersion` se utiliza para llevar un control de las modificaciones usando lo que se llama _optimistic concurrency_: para modificarse, los objetos no son bloqueados.  En su lugar, cuando se solicita la grabación del objeto modificado se comprueba su `resourceVersion`.  Si el objeto ha sido realmente modificado se obtendrá un código "HTTP 409 Conflict".  En ese caso `etcd` se encarga de guardar el objeto nuevo.
    * Las peticiones que no modifican un objeto, como GET, no modifican el valor `resourceVersion` (y recibirán, por tanto, un "HTTP 200 OK").
    * En la base de datos de `etcd` el valor de `resourceVersion` se corresponde con `modifiedIndex`.  Es un valor unívoco por _namespace_, tipo (_kind_) y servidor.


10.  De la colección de datos recibida vamos a quedarnos, de momento, sólo con la entrada "items".  Como resulta ser un _array_, vamos a trabajar sólo con el primer elemento:

     ~~~shell
     ...$ cat $I | jq ".items[0].metadata"
     ...$ cat $I | jq ".items[0].metadata.name"
     "default"
     ~~~


11.  Prácticamente lo tenemos.  Nos quedamos con el campo "name" de los metadatos de cada elemento del _array_ "items":

     ~~~shell
     ...$ cat $I | jq ".items[].metadata.name"
     "default"
     "kube-node-lease"
     "kube-public"
     "kube-system"
     "kubernetes-dashboard"
     ~~~


12. Podemos extraer algún dato más:

    ~~~shell
    ...$ cat $I | jq ".items[].metadata | {name,uid,resourceVersion}"
    ~~~



# Conclusiones

Kubernetes emplea intensivamente un API REST.  De hecho, toda la arquitectura se enfoca, y funciona, por ese API.  Saber cómo funciona el API y como podemos interrogarla nos dará la posibilidad de conocer más detalles de los distintos objetos.


<center>— ♠ —</center>


<BR>
<BR>
## Notas a pie de página

[^1]: En los laboratorios previos hemos usado ficheros en formato [YAML](https://es.wikipedia.org/wiki/YAML).  En realidad, hay un proceso interno de conversión hacia (y desde) JSON.
[^2]: En mayor o menor medida, las APIs están empezando a usar [protobuf](https://es.wikipedia.org/wiki/Protocol_Buffers), el formato de serialización promovido por Google.
[^3]: De hecho, si no estamos autenticados hay muy pocas operaciones admitidas.