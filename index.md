---
title: Kubernetes
description: Curso de Kubernetes
permalink: /
---

# Curso de Kubernetes

* Transparencias
  * {% attach_file {"file_name": "/slides_pdf/01-introduccion.pdf", "title":"01: Introducción.  Certificaciones."} %}
  * {% attach_file {"file_name": "/slides_pdf/02-que-es-k8s.pdf", "title":"02: Qué es Kubernetes"} %}
  * {% attach_file {"file_name": "/slides_pdf/03-arquitectura-k8s.pdf", "title":"03: Arquitectura"} %}
  * {% attach_file {"file_name": "/slides_pdf/04-interfaces.pdf", "title":"04: Interfaces"} %}
  * {% attach_file {"file_name": "/slides_pdf/05-objetos.pdf", "title":"05: Objetos"} %}
  * {% attach_file {"file_name": "/slides_pdf/06-mas-objetos.pdf", "title":"06: Aún más objetos"} %}
  * {% attach_file {"file_name": "/slides_pdf/07-namespaces.pdf", "title":"07: Namespaces"} %}

* Documentación pública
  * [Docker](https://docs.docker.com/)
  * [Kubernetes](https://kubernetes.io/docs/)

* Enlaces extra (genéricos)
  * [Blog](https://www.docker.com/blog/), [foros](https://forums.docker.com/) y [canal en YouTube](https://www.youtube.com/user/dockerrun) oficiales de Docker.
  * [Foro de Docker](https://stackoverflow.com/questions/tagged/docker) en StackOverflow.
  * [Blog](https://kubernetes.io/blog/), [canal en Slack](https://slack.k8s.io/) y [canal en YouTube](https://www.youtube.com/channel/UCZ2bu0qutTOM0tHYa_jkIwg) de Kubernetes.

* Sobre certificaciones
  * Certificaciones CNCF: <https://www.cncf.io/training/certification/>
  * "KCNA study course" (en freeCodeCamp): <https://www.youtube.com/watch?v=AplluksKvzI>
  * Temario (curricula): <https://github.com/cncf/curriculum>
  * Canal en Slack sobre certificaciones de Kubernetes: [#certifications](https://cloud-native.slack.com/archives/CLQT6RZAM)
